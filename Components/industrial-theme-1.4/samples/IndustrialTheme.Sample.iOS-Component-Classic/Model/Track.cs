using System;
#if __UNIFIED__
using UIKit;
#else
using MonoTouch.UIKit;
#endif

namespace ThemeSample
{
	public class Track
	{
		public string TrackName
		{
			get;
			set;
		}
		
		public string ArtistName
		{
			get;
			set;
		}
		
		public string Length
		{
			get;
			set;
		}
		
		public string Genre
		{
			get;
			set;
		}
		
		public UIImage AlbumImage
		{
			get;
			set;
		}
		
		public UIImage AlbumImageLarge
		{
			get;
			set;
		}
	}
}