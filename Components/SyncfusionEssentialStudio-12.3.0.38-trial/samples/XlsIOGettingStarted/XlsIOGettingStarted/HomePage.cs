#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.Reflection;
using Xamarin.Forms;

namespace XlsIO
{
    class HomePage : ContentPage
    {
        public HomePage()
        {
            // Define command for the items in the TableView.
            Command<Type> navigateCommand =
                new Command<Type>(async (Type pageType) =>
                {
                    // Get all the constructors of the page type.
                    IEnumerable<ConstructorInfo> constructors =
                            pageType.GetTypeInfo().DeclaredConstructors;

                    foreach (ConstructorInfo constructor in constructors)
                    {
                        // Check if the constructor has no parameters.
                        if (constructor.GetParameters().Length == 0)
                        {
                            // If so, instantiate it, and navigate to it.
                            Page page = (Page)constructor.Invoke(null);
                            await this.Navigation.PushAsync(page);
                            break;
                        }
                    }
                });
            
            this.Title = "Essential XlsIO";
            this.Content = new TableView
            {
                Intent = TableIntent.Menu,
                Root = new TableRoot("Essential XlsIO")
                {
                        new TableSection("")
                        {
                            new TextCell
                            {
                                Text = "Create Spreadsheet",
                                Command = navigateCommand,
                                CommandParameter = typeof(CreateSpreadsheetPage)
                            },

                            new TextCell
                            {
                                Text = "Charts",
                                Command = navigateCommand,
                                CommandParameter = typeof(ChartsPage)
                            },

                            new TextCell
                            {
                                Text = "Formulas",
                                Command = navigateCommand,
                                CommandParameter = typeof(FormulasPage)
                            },

                            new TextCell
                            {
                                Text = "Import XML",
                                Command = navigateCommand,
                                CommandParameter = typeof(ImportXMLPage)
                            },

                            new TextCell
                            {
                                Text = "Import Business Objects",
                                Command = navigateCommand,
                                CommandParameter = typeof(ImportBusinessObjectsPage)
                            },
                        },  
                }
            };
        }
    }
}
