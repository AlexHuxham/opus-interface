#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using Xamarin.Forms;
using System.IO;
using System.Reflection;
using Syncfusion.DocIO.DLS;
using Syncfusion.DocIO;
using System.Collections.Generic;

namespace DocIO
{
    class LetterFormatting : ContentPage
    {
        public LetterFormatting()
        {
            Label title = new Label
            {
                Text = "Letter Formatting",
            };

            Label description = new Label
            {
                Text = "\nThe mail merge function allows you to fill the mail merge fields in the template document with data from the data source. \n\nThis sample demonstrates to create a letter using mail merge functionality with business objects. \n\nPlease click the Generate Word  button to save and view the Word document generated by Essential DocIO.\n\n",
            };

            Button button = new Button
            {
                Text = "Generate Word",
            };
            button.Clicked += OnButtonClicked;

            // Accomodate iPhone status bar.
            this.Padding = new Thickness(20, Device.OnPlatform(20, 20, 20), 20, 20);

            if (Device.OS == TargetPlatform.iOS)
            {
                title.Font = Font.SystemFontOfSize(20);
                description.Font = Font.SystemFontOfSize(15);
            }
            else if (Device.OS == TargetPlatform.WinPhone)
            {
                title.Font = Font.SystemFontOfSize(35);
            }
            else if (Device.OS == TargetPlatform.Android)
            {
                title.Font = Font.SystemFontOfSize(25);
            }

            // Build the page.
            this.Content = new StackLayout
            {
                Children = 
                {
                    title,
                    description,
                    button,
                }
            };
        }

        void OnButtonClicked(object sender, EventArgs e)
        {
            Assembly assembly = typeof(App).GetTypeInfo().Assembly;
            // Creating a new document.
            WordDocument document = new WordDocument();
            Stream inputStream = assembly.GetManifestResourceStream("DocIO.Templates.Letter Formatting.docx");
            //Open Template document
            document.Open(inputStream, FormatType.Word2013);
            inputStream.Dispose();
            List<Customer> source = new List<Customer>();
            source.Add(new Customer("ALFKI", "Alfreds Futterkiste", "Maria Anders", "Sales Representative", "Obere Str. 57", "Berlin", "12209", "Germany", "030-0074321", "030-0076545"));
            //source.Add(new Customer("ANATR", "Ana Trujillo Emparedados y helados", "Ana Trujillo", "Owner", "Avda. de la Constitución 2222", "México D.F.", "05021", "Mexico", "(5) 555-4729", "(5) 555-3745"));
            document.MailMerge.Execute(source);
            
            MemoryStream stream = new MemoryStream();
            document.Save(stream, FormatType.Word2013);
            document.Close();

            Xamarin.Forms.DependencyService.Get<ISave>().Save("LetterFormatting.docx", "application/msword", stream);
        }
    }
    public class Customer
    {
        #region fields
        string m_customerID;
        string m_companyName;
        string m_contactName;
        string m_contactTitle;
        string m_address;
        string m_city;
        string m_postalCode;
        string m_country;
        string m_phone;
        string m_fax;
        #endregion

        #region properties
        public string CustomerID
        {
            get
            {
                return m_customerID;
            }
            set
            {
                m_customerID = value;
            }
        }
        public string CompanyName
        {
            get
            {
                return m_companyName;
            }
            set
            {
                m_companyName = value;
            }
        }
        public string ContactName
        {
            get
            {
                return m_contactName;
            }
            set
            {
                m_contactName = value;
            }
        }
        public string ContactTitle
        {
            get
            {
                return m_contactTitle;
            }
            set
            {
                m_contactTitle = value;
            }
        }
        public string Address
        {
            get
            {
                return m_address;
            }
            set
            {
                m_address = value;
            }
        }
        public string City
        {
            get
            {
                return m_city;
            }
            set
            {
                m_city = value;
            }
        }
        public string PostalCode
        {
            get
            {
                return m_postalCode;
            }
            set
            {
                m_postalCode = value;
            }
        }
        public string Country
        {
            get
            {
                return m_country;
            }
            set
            {
                m_country = value;
            }
        }
        public string Phone
        {
            get
            {
                return m_phone;
            }
            set
            {
                m_phone = value;
            }
        }
        public string Fax
        {
            get
            {
                return m_fax;
            }
            set
            {
                m_fax = value;
            }
        }
        #endregion

        #region constructor
        public Customer()
        { }

        public Customer(string customerID, string companyName, string contactName, string contactTitle, string address, string city, string postalCode, string country, string phone, string fax)
        {
            m_customerID = customerID;
            m_companyName = companyName;
            m_contactName = contactName;
            m_contactTitle = contactTitle;
            m_address = address;
            m_city = city;
            m_postalCode = postalCode;
            m_country = country;
            m_phone = phone;
            m_fax = fax;
        }
        #endregion
    }
}
