#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.IO;
using Xamarin.Forms;
using Syncfusion.DocIO.DLS;
using Syncfusion.DocIO;
using System.Reflection;

namespace DocIO
{
    class BookMarkNavigation : ContentPage
    {
        public BookMarkNavigation()
        {
            Label title = new Label
            {
                Text = "Bookmark Navigation",
            };

            Label description = new Label
            {
                Text = "\nBookmarkNavigator is used to navigate between bookmarks and manipulate bookmark content in the Word document. \n\nThis sample demonstrates how to navigate and manipulate bookmark contents in Word document using Essential DocIO. \n\nPlease click the Generate Word  button to save and view the Word document generated by Essential DocIO.\n\n",
            };

            Button button = new Button
            {
                Text = "Generate Word",
            };
            button.Clicked += OnButtonClicked;

            // Accomodate iPhone status bar.
            this.Padding = new Thickness(20, Device.OnPlatform(20, 20, 20), 20, 20);

            if (Device.OS == TargetPlatform.iOS)
            {
                title.Font = Font.SystemFontOfSize(20);
                description.Font = Font.SystemFontOfSize(15);
            }
            else if (Device.OS == TargetPlatform.WinPhone)
            {
                title.Font = Font.SystemFontOfSize(35);
            }
            else if (Device.OS == TargetPlatform.Android)
            {
                title.Font = Font.SystemFontOfSize(25);
            }

            // Build the page.
            this.Content = new StackLayout
            {
                Children = 
                {
                    title,
                    description,
                    button,
                }
            };
        }

        void OnButtonClicked(object sender, EventArgs e)
        {
            Assembly assembly = typeof(App).GetTypeInfo().Assembly;
            // Creating a new document.
            WordDocument document = new WordDocument();
            Stream inputStream = assembly.GetManifestResourceStream("DocIO.Templates.Bookmark_Template.doc");

            // Open an existing template document.
            document.Open(inputStream, FormatType.Doc);
            inputStream.Dispose();
            // Creating a bookmark navigator. Which help us to navigate through the 
            // bookmarks in the template document.
            BookmarksNavigator bk = new BookmarksNavigator(document);

            // Move to the header bookmark.
            bk.MoveToBookmark("HeaderBookmark");

            // Get the table in the document.
            TextBodyPart bodyPart = new TextBodyPart(document);
            bodyPart.BodyItems.Add(document.Sections[0].Tables[0] as WTable);
            // Move to the DocIO bookmark.
            bk.MoveToBookmark("HeaderBookmark");
            // Replacing content with table.
            bk.ReplaceBookmarkContent(bodyPart);


            // Move to the Essential_DocIO bookmark.
            bk.MoveToBookmark("Northwind");
            // Get the content of Essential_DocIO bookmark.
            bodyPart = bk.GetBookmarkContent();
            // Move to the DocIO bookmark.
            bk.MoveToBookmark("Northwind_Description");
            // Replacing content of DocIO bookmark.
            bk.ReplaceBookmarkContent(bodyPart);


            // Move to the Essential_PDF bookmark.
            bk.MoveToBookmark("Information");
            // Get the content of Essential_PDF bookmark.
            bodyPart = bk.GetBookmarkContent();
            // Move to the PDF bookmark.
            bk.MoveToBookmark("Northwind_Information");
            // Replacing content of PDF bookmark.
            bk.ReplaceBookmarkContent(bodyPart);


            // Move to the Text bookmark.
            bk.MoveToBookmark("Text");
            bk.DeleteBookmarkContent(true);
            // Inserting text inside the bookmark. This will preserve the source formatting
            bk.InsertText("Northwind Database contains the following table:");
            #region tableinsertion
            WTable tbl = new WTable(document);
            tbl.TableFormat.Borders.BorderType = BorderStyle.None;
            tbl.TableFormat.IsAutoResized = true;
            tbl.ResetCells(8, 2);
            IWParagraph paragraph;
            tbl.Rows[0].IsHeader = true;
            paragraph = tbl[0, 0].AddParagraph();
            paragraph.AppendText("Suppliers");
            paragraph.BreakCharacterFormat.FontName = "Calibri";
            paragraph.BreakCharacterFormat.FontSize = 10;
            
            paragraph = tbl[0, 1].AddParagraph();
            paragraph.AppendText("1");
            paragraph.BreakCharacterFormat.FontName = "Calibri";
            paragraph.BreakCharacterFormat.FontSize = 10;

            paragraph = tbl[1, 0].AddParagraph();
            paragraph.AppendText("Customers");
            paragraph.BreakCharacterFormat.FontName = "Calibri";
            paragraph.BreakCharacterFormat.FontSize = 10;

            paragraph = tbl[1, 1].AddParagraph();
            paragraph.AppendText("1");
            paragraph.BreakCharacterFormat.FontName = "Calibri";
            paragraph.BreakCharacterFormat.FontSize = 10;

            paragraph = tbl[2, 0].AddParagraph();
            paragraph.AppendText("Employees");
            paragraph.BreakCharacterFormat.FontName = "Calibri";
            paragraph.BreakCharacterFormat.FontSize = 10;

            paragraph = tbl[2, 1].AddParagraph();
            paragraph.AppendText("3");
            paragraph.BreakCharacterFormat.FontName = "Calibri";
            paragraph.BreakCharacterFormat.FontSize = 10;

            paragraph = tbl[3, 0].AddParagraph();
            paragraph.AppendText("Products");
            paragraph.BreakCharacterFormat.FontName = "Calibri";
            paragraph.BreakCharacterFormat.FontSize = 10;

            paragraph = tbl[3, 1].AddParagraph();
            paragraph.AppendText("1");
            paragraph.BreakCharacterFormat.FontName = "Calibri";
            paragraph.BreakCharacterFormat.FontSize = 10;

            paragraph = tbl[4, 0].AddParagraph();
            paragraph.AppendText("Inventory");
            paragraph.BreakCharacterFormat.FontName = "Calibri";
            paragraph.BreakCharacterFormat.FontSize = 10;

            paragraph = tbl[4, 1].AddParagraph();
            paragraph.AppendText("2");
            paragraph.BreakCharacterFormat.FontName = "Calibri";
            paragraph.BreakCharacterFormat.FontSize = 10;

            paragraph = tbl[5, 0].AddParagraph();
            paragraph.AppendText("Shippers");
            paragraph.BreakCharacterFormat.FontName = "Calibri";
            paragraph.BreakCharacterFormat.FontSize = 10;

            paragraph = tbl[5, 1].AddParagraph();
            paragraph.AppendText("1");
            paragraph.BreakCharacterFormat.FontName = "Calibri";
            paragraph.BreakCharacterFormat.FontSize = 10;

            paragraph = tbl[6, 0].AddParagraph();
            paragraph.AppendText("PO Transactions");
            paragraph.BreakCharacterFormat.FontName = "Calibri";
            paragraph.BreakCharacterFormat.FontSize = 10;

            paragraph = tbl[6, 1].AddParagraph();
            paragraph.AppendText("3");
            paragraph.BreakCharacterFormat.FontName = "Calibri";
            paragraph.BreakCharacterFormat.FontSize = 10;

            paragraph = tbl[7, 0].AddParagraph();
            paragraph.AppendText("Sales Transactions");
            paragraph.BreakCharacterFormat.FontName = "Calibri";
            paragraph.BreakCharacterFormat.FontSize = 10;

            paragraph = tbl[7, 1].AddParagraph();
            paragraph.AppendText("7");
            paragraph.BreakCharacterFormat.FontName = "Calibri";
            paragraph.BreakCharacterFormat.FontSize = 10;


            bk.InsertTable(tbl);
            #endregion
            bk.MoveToBookmark("Image");
            bk.DeleteBookmarkContent(true);
            // Inserting image to the bookmark.
            IWPicture pic = bk.InsertParagraphItem(ParagraphItemType.Picture) as WPicture;
            inputStream = assembly.GetManifestResourceStream("DocIO.Templates.Northwind.png");
            pic.LoadImage(inputStream);
            inputStream.Dispose();
            pic.WidthScale = 50f;  // It reduce the image size because it don't fit 
            pic.HeightScale = 75f; // in document page.


            #region Saving Document
            MemoryStream stream = new MemoryStream();
            document.Save(stream, FormatType.Word2013);
            document.Close();

            Xamarin.Forms.DependencyService.Get<ISave>().Save("BookMarkNavigation.docx", "application/msword", stream);
            #endregion
        }
    }
}
