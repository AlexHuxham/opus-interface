#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Syncfusion.SfTreeMap.XForms;
using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace TreeMapGettingStarted
{
	public class MainPage : ContentPage
	{
        SfTreeMap tree;
		public MainPage ()
		{
             tree = new SfTreeMap();
            tree.WeightValuePath = "Weight";
            tree.ColorValuePath = "ColorWeight";
            tree.LeafItemSettings = new LeafItemSettings();
            tree.LeafItemSettings.LabelStyle = new Style() { Font = Font.SystemFontOfSize(14), Color = Color.White };
            tree.LeafItemSettings.LabelPath = "Label";
            ObservableCollection<Range> ranges = new ObservableCollection<Range>();
            ranges.Add(new Range() { LegendLabel = "1 % Growth", From = 0, To = 1, Color = Color.FromHex("#77D8D8") });
            ranges.Add(new Range() { LegendLabel = "2 % Growth", From = 0, To = 2, Color = Color.FromHex("#AED960") });
            ranges.Add(new Range() { LegendLabel = "3 % Growth", From = 0, To = 3, Color = Color.FromHex("#FFAF51") });
            ranges.Add(new Range() { LegendLabel = "4 % Growth", From = 0, To = 4, Color = Color.FromHex("#F3D240") });
            tree.LeafItemColorMapping = new RangeColorMapping() { Ranges = ranges };
            Size legendSize = Device.OnPlatform(iOS: new Size(300, 60), Android: new Size(300, 60), WinPhone: new Size(420, 75));
            Size iconSize = Device.OnPlatform(iOS: new Size(17, 17), Android: new Size(22, 22), WinPhone: new Size(15, 15));
			Color legendColor= Device.OnPlatform(iOS: Color.Gray, Android: Color.White, WinPhone: Color.White);
			tree.LegendSettings = new LegendSettings()
            {
                LabelStyle = new Style()
                {
                    Font = Font.SystemFontOfSize(12),
					Color = legendColor
                },
                IconSize = iconSize,
                ShowLegend = true,
                Size = legendSize
            };
            GetPopulationData();
            tree.Items = PopulationDetails;
            double labelHeight = Device.OnPlatform(iOS: 20, Android: 35, WinPhone: 35);
            Label label = new Label() { Text = "TreeMap Getting Started", HeightRequest = labelHeight, Font = Font.OfSize("Bold", 25) };
            var mainStack = new StackLayout
            {
                Spacing = 10,
				Padding= Device.OnPlatform(iOS:new Thickness(2,20,2,0),Android:new Thickness(0),WinPhone:new Thickness(0)),
                Children = { label, tree }
            };
            mainStack.SizeChanged += mainStack_SizeChanged;
            this.Content = mainStack;
           
		}

        void mainStack_SizeChanged(object sender, EventArgs e)
        {
            tree.HeightRequest = (sender as StackLayout).Height - 50;   
        }
         void  GetPopulationData()
		{
            PopulationDetails = new ObservableCollection<TreeMapItem>();
			PopulationDetails.Add(new TreeMapItem() { Label = "Indonesia", ColorWeight = 3, Weight = 237641326 });
			PopulationDetails.Add(new TreeMapItem() { Label = "Russia", ColorWeight = 2, Weight = 152518015 });
			PopulationDetails.Add(new TreeMapItem() { Label = "United States", ColorWeight = 4, Weight = 315645000 });
			PopulationDetails.Add(new TreeMapItem() { Label = "Mexico", ColorWeight = 2, Weight = 112336538 });
			PopulationDetails.Add(new TreeMapItem() { Label = "Nigeria", ColorWeight = 2, Weight = 170901000 });
			PopulationDetails.Add(new TreeMapItem() { Label = "Egypt", ColorWeight = 1, Weight = 83661000 });
			PopulationDetails.Add(new TreeMapItem() { Label = "Germany", ColorWeight = 1, Weight = 81993000 });
			PopulationDetails.Add(new TreeMapItem() { Label = "France", ColorWeight = 1, Weight = 65605000 });
			PopulationDetails.Add(new TreeMapItem() { Label = "UK", ColorWeight = 1, Weight = 63181775 });
		}

		public ObservableCollection<TreeMapItem> PopulationDetails
		{
			get;
			set;
		}

	}
	
}

