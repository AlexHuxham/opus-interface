#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Syncfusion.SfChart.XForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace ChartGettingStarted
{
    public class App
    {
        public static Page GetMainPage()
        {
            return Device.OnPlatform(iOS: new NavigationPage(new MainPage()) as Page, Android: new NavigationPage(new MainPage()) as Page, WinPhone: new MainPage_WP() as Page); 
            //return new OHLC ();
        }
    }
}
