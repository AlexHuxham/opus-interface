#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Syncfusion.SfChart.XForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using Xamarin.Forms;
namespace ChartGettingStarted
{
    public class MainPage : ContentPage
    {
        public MainPage()
        {
           
           
            this.Title = "Essential Chart";
            System.Collections.Generic.List<Item> items = new List<Item>();
            items.Add(new Item() { Name = "Column", ItemType = typeof(Column) });
            items.Add(new Item() { Name = "Line", ItemType = typeof(Line) });
            items.Add(new Item() { Name = "Spline", ItemType = typeof(Spline) });
            items.Add(new Item() { Name = "Pie", ItemType = typeof(Pie) });
            items.Add(new Item() { Name = "Stacking Column", ItemType = typeof(StackingColumn) });
            items.Add(new Item() { Name = "Stacking Column 100", ItemType = typeof(StackingColumn100) });
            items.Add(new Item() { Name = "Bar", ItemType = typeof(Bar) });
            items.Add(new Item() { Name = "Stacking Bar", ItemType = typeof(StackingBar) });
            items.Add(new Item() { Name = "Stacking Bar 100", ItemType = typeof(StackingBar100) });

            items.Add(new Item() { Name = "Area", ItemType = typeof(Area) });
            items.Add(new Item() { Name = "Stacking Area", ItemType = typeof(StackingArea) });
            items.Add(new Item() { Name = "Stacking Area 100", ItemType = typeof(StackingArea100) });
            items.Add(new Item() { Name = "Candle", ItemType = typeof(Candle) });
            items.Add(new Item() { Name = "HiLoOpenClose", ItemType = typeof(OHLC) });
            items.Add(new Item() { Name = "Live Update", ItemType = typeof(LiveUpdate) });

            items.Add(new Item() { Name = "Category Axis", ItemType = typeof(Category) });
            items.Add(new Item() { Name = "Numerical Axis", ItemType = typeof(Numerical) });
            items.Add(new Item() { Name = "DateTime Axis", ItemType = typeof(Date) });
            items.Add(new Item() { Name = "Multiple Axis", ItemType = typeof(MultipleSeries) });
            ListView listview = new ListView
            {
                ItemsSource = items,
                
            
            };
            listview.ItemSelected += listview_ItemSelected;
            listview.ItemTemplate = new DataTemplate(typeof(TextCell));
            listview.ItemTemplate.SetBinding(TextCell.TextProperty, "Name");
            listview.ItemTemplate.SetBinding(TextCell.CommandParameterProperty, "ItemType");
            //listview.ItemTemplate.SetBinding(TextCell.CommandProperty, "ItemCommand");
            listview.HeightRequest = 100;



            this.Content = listview ;
        }

       async void listview_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            Item item = e.SelectedItem as Item;

            IEnumerable<ConstructorInfo> constructors =
                             item.ItemType.GetTypeInfo().DeclaredConstructors;

            foreach (ConstructorInfo constructor in constructors)
            {
                // Check if the constructor has no parameters.
                if (constructor.GetParameters().Length == 0)
                {
                    // If so, instantiate it, and navigate to it.
                    Page page = (Page)constructor.Invoke(null);
                    await this.Navigation.PushAsync(page);
                    break;
                }
            }
        }

       public static ObservableCollection<ChartDataPoint> GetAreaData()
       {
           ObservableCollection<ChartDataPoint> datas = new ObservableCollection<ChartDataPoint>();
           datas.Add(new ChartDataPoint("2010", 45));
           datas.Add(new ChartDataPoint("2011", 56));
           datas.Add(new ChartDataPoint("2012", 23));
           datas.Add(new ChartDataPoint("2013", 43));
           datas.Add(new ChartDataPoint("2014", Double.NaN));
           datas.Add(new ChartDataPoint("2015", 54));
           datas.Add(new ChartDataPoint("2016", 43));
           datas.Add(new ChartDataPoint("2017", 23));
           datas.Add(new ChartDataPoint("2018", 34));
           return datas;
       }

        public static ObservableCollection<ChartDataPoint> GetData1()
        {
            ObservableCollection<ChartDataPoint> datas = new ObservableCollection<ChartDataPoint>();
            datas.Add(new ChartDataPoint("2010", 45));
            datas.Add(new ChartDataPoint("2011", 86));
            datas.Add(new ChartDataPoint("2012", 23));
            datas.Add(new ChartDataPoint("2013", 43));
            datas.Add(new ChartDataPoint("2014", 54));
            return datas;
        }
        public static ObservableCollection<ChartDataPoint> GetCategoryData()
        {
            ObservableCollection<ChartDataPoint> datas = new ObservableCollection<ChartDataPoint>();
            datas.Add(new ChartDataPoint("Bentley", 54));
            datas.Add(new ChartDataPoint("Audi", 24));
            datas.Add(new ChartDataPoint("BMW", 53));
            datas.Add(new ChartDataPoint("Jaguar", 63));
            datas.Add(new ChartDataPoint("Skoda", 35));
            return datas;
        }

        public static ObservableCollection<ChartDataPoint> GetNumericalData()
        {
            ObservableCollection<ChartDataPoint> datas = new ObservableCollection<ChartDataPoint>();
            datas.Add(new ChartDataPoint(1, 54));
            datas.Add(new ChartDataPoint(2, 24));
            datas.Add(new ChartDataPoint(3, 53));
            datas.Add(new ChartDataPoint(4, 63));
            datas.Add(new ChartDataPoint(5, 35));
            return datas;
        }

        public static ObservableCollection<ChartDataPoint> GetData2()
        {
            ObservableCollection<ChartDataPoint> datas = new ObservableCollection<ChartDataPoint>();
            datas.Add(new ChartDataPoint("2010", 54));
            datas.Add(new ChartDataPoint("2011", 24));
            datas.Add(new ChartDataPoint("2012", 53));
            datas.Add(new ChartDataPoint("2013", 63));
            datas.Add(new ChartDataPoint("2014", 35));
            return datas;
        }

        public static ObservableCollection<ChartDataPoint> GetData3()
        {
            ObservableCollection<ChartDataPoint> datas = new ObservableCollection<ChartDataPoint>();
            datas.Add(new ChartDataPoint("2010", 14));
            datas.Add(new ChartDataPoint("2011", 54));
            datas.Add(new ChartDataPoint("2012", 23));
            datas.Add(new ChartDataPoint("2013", 53));
            datas.Add(new ChartDataPoint("2014", 25));
            return datas;
        }

        public static ObservableCollection<ChartDataPoint> GetFinancialData()
        {
            ObservableCollection<ChartDataPoint> datas = new ObservableCollection<ChartDataPoint>();
            datas.Add(new ChartDataPoint("2010", 873.8, 878.85, 855.5, 860.5));
            datas.Add(new ChartDataPoint("2011", 861, 868.4, 835.2, 843.45));
            datas.Add(new ChartDataPoint("2012", 846.15, 853, 838.5, 847.5));
            datas.Add(new ChartDataPoint("2013", 846, 860.75, 841, 855));
            datas.Add(new ChartDataPoint("2014", 841, 845, 827.85, 838.65));
            return datas;
        }

        public static ObservableCollection<ChartDataPoint> GetDateTimValue()
        {
            ObservableCollection<ChartDataPoint> datas = new ObservableCollection<ChartDataPoint>();
            datas.Add(new ChartDataPoint(new DateTime(2012, 01, 1), 14));
            datas.Add(new ChartDataPoint(new DateTime(2012, 01, 2), 54));
            datas.Add(new ChartDataPoint(new DateTime(2012, 01, 3), 23));
            datas.Add(new ChartDataPoint(new DateTime(2012, 01, 4), 53));
            datas.Add(new ChartDataPoint(new DateTime(2012, 01, 5), 25));
            return datas;
        }
    }
    public class Item
    {
        public string Name { get; set; }
        public Type ItemType { get; set; }
        public Command ItemCommand { get; set; }

    }
}