#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Syncfusion.SfChart.XForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ChartGettingStarted
{
    public class LiveUpdate : ContentPage
    {
        public LiveUpdate()
        {
            Content = GetChart();
        }

        ObservableCollection<ChartDataPoint> data = new ObservableCollection<ChartDataPoint>();
        ObservableCollection<ChartDataPoint> data2 = new ObservableCollection<ChartDataPoint>();
        int wave1 = 0;
        int wave2 = 180;

        SfChart chart;
        private SfChart GetChart()
        {
            LoadData();

            chart = new SfChart();
            chart.PrimaryAxis = new NumericalAxis();
            var axis = new NumericalAxis();
            axis.Minimum = -1.5;
            axis.Maximum = 1.5;
            chart.SecondaryAxis = axis;
            axis.LabelStyle.LabelsPosition = AxisElementPosition.Outside;
            axis.TickPosition = AxisElementPosition.Outside;

            var lineSeries = new FastLineSeries();
            lineSeries.ItemsSource = data;

            chart.Series.Add(lineSeries);

            UpdateData();
            AddSeries();
            return chart;
        }

        void LoadData()
        {
            for (int i = 0; i < 300; i++)
            {
                data.Add(new ChartDataPoint(i, Math.Sin(wave1 * (Math.PI / 180.0))));
                wave1++;
            }

            for (int i = 0; i < 300; i++)
            {
                data2.Add(new ChartDataPoint(i, Math.Sin(wave2 * (Math.PI / 180.0))));
                wave2++;
            }

            wave1 = data.Count;
        }

        async void AddSeries()
        {
            await Task.Delay(400);
            var fastLine = new FastLineSeries();
            chart.Series.Add(fastLine);
            fastLine.ItemsSource = data2;
        }

        async void UpdateData()
        {
            await Task.Delay(10);
            data.RemoveAt(0);
            data.Add(new ChartDataPoint(wave1, Math.Sin(wave1 * (Math.PI / 180.0))));
            wave1++;

            data2.RemoveAt(0);
            data2.Add(new ChartDataPoint(wave1, Math.Sin(wave2 * (Math.PI / 180.0))));
            wave2++;
            UpdateData();
        }
    }
}