#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Syncfusion.SfChart.XForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ChartGettingStarted
{
    public class StackingColumn100 : ContentPage
    {
        public StackingColumn100()
        {
            Content = GetChart();
        }

        private static SfChart GetChart()
        {
            SfChart chart = new SfChart();
            chart.PrimaryAxis = new CategoryAxis() { LabelPlacement = LabelPlacement.BetweenTicks };
            chart.SecondaryAxis = new NumericalAxis() { ShowMajorGridLines = false };

            chart.Series.Add(new StackingColumn100Series()
            {
                DataMarker = new ChartDataMarker(),
                ItemsSource = MainPage.GetData1(),
                DataMarkerPosition = Syncfusion.SfChart.XForms.DataMarkerPosition.Center,
            });

            chart.Series.Add(new StackingColumn100Series()
            {
                DataMarker = new ChartDataMarker(),
                ItemsSource = MainPage.GetData2(),
                DataMarkerPosition = Syncfusion.SfChart.XForms.DataMarkerPosition.Center,
            });

            chart.Series.Add(new StackingColumn100Series()
            {
                DataMarker = new ChartDataMarker(),
                ItemsSource = MainPage.GetData3(),
                DataMarkerPosition = Syncfusion.SfChart.XForms.DataMarkerPosition.Center,
            });
            return chart;
        }
    }
}
