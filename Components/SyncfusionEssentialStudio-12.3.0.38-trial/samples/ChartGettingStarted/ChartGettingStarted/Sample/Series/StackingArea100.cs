#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Syncfusion.SfChart.XForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ChartGettingStarted
{
    public class StackingArea100 : ContentPage
    {
        public StackingArea100()
        {
            Content = GetChart();
        }

        private SfChart GetChart()
        {
            chart = new SfChart();
            chart.PrimaryAxis = new CategoryAxis();
            chart.SecondaryAxis = new NumericalAxis();

            chart.Series.Add(new StackingArea100Series()
            {
                StrokeWidth = 5,
                Color = Xamarin.Forms.Color.FromHex("0x99FEBE17"),
                StrokeColor = Xamarin.Forms.Color.FromHex("0xFFFEBE17"),
            });

            chart.Series.Add(new StackingArea100Series()
            {
                StrokeWidth = 5,
                Color = Xamarin.Forms.Color.FromHex("0x994F4838"),
                StrokeColor = Xamarin.Forms.Color.FromHex("0xFF4F4838"),
            });

            chart.Series.Add(new StackingArea100Series()
            {
                StrokeWidth = 5,
                Color = Xamarin.Forms.Color.FromHex("0x99C15146"),
                StrokeColor = Xamarin.Forms.Color.FromHex("0xFFC15146"),
                ItemsSource = GetData(2)
            });

            UpdateData();
            return chart;
        }

        int index = 1;
        SfChart chart;

        async void UpdateData()
        {
            await Task.Delay(300);
            chart.Series[index].ItemsSource = GetData(index);
            index--;
            if (index >= 0)
                UpdateData();
        }

        ObservableCollection<ChartDataPoint> GetData(int index)
        {
            if (index == 0)
                return MainPage.GetData1();
            if (index == 1)
                return MainPage.GetData2();
            return MainPage.GetData3();
        }
    }
}
