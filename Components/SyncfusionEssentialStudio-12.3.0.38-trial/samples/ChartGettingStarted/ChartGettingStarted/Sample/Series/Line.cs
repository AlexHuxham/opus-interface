#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Syncfusion.SfChart.XForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ChartGettingStarted
{
    public class Line : ContentPage
    {
        public Line()
        {
            Content = GetChart();
        }

        private static SfChart GetChart()
        {
            var chart = new SfChart();
            chart.PrimaryAxis = new CategoryAxis();
            chart.PrimaryAxis.PlotOffset = 20;
            chart.SecondaryAxis = new NumericalAxis();
            var lineSeries = new LineSeries();
            lineSeries.DataMarker = new ChartDataMarker();
            lineSeries.DataMarker.ConnectorLineStyle.ConnectorHeight = 10;
            lineSeries.DataMarker.ConnectorLineStyle.StrokeWidth = 0;
            lineSeries.ItemsSource = MainPage.GetData1();
            lineSeries.StrokeWidth = 3;

            chart.Series.Add(lineSeries);
            return chart;
        }
    }
}
