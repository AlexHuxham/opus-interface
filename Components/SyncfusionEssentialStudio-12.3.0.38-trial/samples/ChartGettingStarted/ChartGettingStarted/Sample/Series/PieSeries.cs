#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Syncfusion.SfChart.XForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace ChartGettingStarted
{
    public class Pie : ContentPage
    {
        public Pie()
        {
            this.Content = GetChart();
        }

        private static SfChart GetChart()
        {
            SfChart chart = new SfChart() { Legend = new ChartLegend() };

            ObservableCollection<ChartDataPoint> datas = new ObservableCollection<ChartDataPoint>();
            datas.Add(new ChartDataPoint("2010", 8000));
            datas.Add(new ChartDataPoint("2011", 8100));
            datas.Add(new ChartDataPoint("2012", 8250));
            datas.Add(new ChartDataPoint("2013", 8600));
            datas.Add(new ChartDataPoint("2014", 8700));

            var pieSeries = new PieSeries()
            {
                ExplodeIndex = 3,
                ItemsSource = datas,
                ConnectorLineType = ConnectorLineType.Bezier,
                DataMarkerPosition = CircularSeriesDataMarkerPosition.OutsideExtended,
                DataMarker = new ChartDataMarker()
            };

            pieSeries.DataMarker.LabelStyle.Margin = new Thickness(5);
            chart.Series.Add(pieSeries);
            return chart;
        }
    }
}
