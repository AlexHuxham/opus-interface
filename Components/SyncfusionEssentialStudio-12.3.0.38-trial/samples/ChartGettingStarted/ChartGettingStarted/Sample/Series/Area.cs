#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Syncfusion.SfChart.XForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ChartGettingStarted
{
    public class Area : ContentPage
    {
        public Area()
        {
            Content = GetChart();
        }

        private static SfChart GetChart()
        {
            SfChart chart = new SfChart();
            chart.PrimaryAxis = new CategoryAxis();
            chart.PrimaryAxis.PlotOffset = 20;
            chart.SecondaryAxis = new NumericalAxis();

            var areaSeries = new AreaSeries()
            {
                StrokeWidth = 5,
                Color = Xamarin.Forms.Color.FromHex("0x90FEBE17"),
                StrokeColor = Xamarin.Forms.Color.FromHex("0xFFFEBE17"),
                ItemsSource = MainPage.GetAreaData()
            };

            chart.Series.Add(areaSeries);
            return chart;
        }
    }
}
