#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ChartGettingStarted 
{
    public class MainPage_WP : TabbedPage
    {
        public MainPage_WP()
        {
            Children.Add(new Column { Title = "Column" });
            Children.Add(new Line() { Title = "Line" });
            Children.Add(new Spline() { Title = "Spline" });
            Children.Add(new Pie() { Title = "Pie" });
            Children.Add(new StackingColumn() { Title = "Stacking Column" });
            Children.Add(new StackingColumn100() { Title = "Stacking Column 100" });
            Children.Add(new Bar() { Title = "Bar" });
            Children.Add(new StackingBar() { Title = "Stacking Bar" });
            Children.Add(new StackingBar100() { Title = "Stacking Bar 100" });
            Children.Add(new Area() { Title = "Area" });
            Children.Add(new StackingArea() { Title = "Stacking Area" });
            Children.Add(new StackingArea100() { Title = "Stacking Area 100" });
            Children.Add(new Candle() { Title = "Candle" });
            Children.Add(new OHLC() { Title = "HiLoOpenClose" });
            Children.Add(new LiveUpdate() { Title = "Live Update" });

            Children.Add(new Category() { Title = "Category Axis" });
            Children.Add(new Numerical() { Title = "Numerical axis" });
            Children.Add(new Date() { Title = "DateTime Axis" });
            Children.Add(new MultipleSeries() { Title = "Multiple axis" });
          
        }
    }
}
