#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Syncfusion.SfChart.XForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ChartGettingStarted
{
    public class Numerical : ContentPage
    {
        public Numerical()
        {
            Content = GetChart();
        }

        private static SfChart GetChart()
        {
            SfChart chart = new SfChart();

            chart.Legend = new ChartLegend();
            chart.Legend.ToggleSeriesVisibility = true;

            var xAxis = new NumericalAxis();
            xAxis.Interval = 1;
            xAxis.Title.Text = "X Axis";
            chart.PrimaryAxis = xAxis;

            var yAxis = new NumericalAxis();
            yAxis.Title.Text = "Y Axis";
            chart.SecondaryAxis = yAxis;
            chart.Series.Clear();

            chart.Series.Add(new ColumnSeries()
            {
                Label = "Column Series",
                ItemsSource = MainPage.GetNumericalData(),
            });
            return chart;
        }
    }
}