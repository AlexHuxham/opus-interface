#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Syncfusion.SfChart.XForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ChartGettingStarted
{
    public class Date : ContentPage
    {
        public Date()
        {
            Content = GetChart();
        }

        private static SfChart GetChart()
        {
            SfChart chart = new SfChart();
            
            chart.Legend = new ChartLegend();
            chart.Legend.ToggleSeriesVisibility = true;

            var dateAxis = new DateTimeAxis()
            {
                EdgeLabelsDrawingMode = Syncfusion.SfChart.XForms.EdgeLabelsDrawingMode.Shift,
                LabelRotationAngle = -45,
            };
            dateAxis.LabelStyle.LabelFormat = "MM/dd/yyyy";

            chart.PrimaryAxis = dateAxis;
            dateAxis.Title.Text = "DateTime Axis";

            var numericalAxis = new NumericalAxis();
            numericalAxis.Title.Text = "Numerical Axis";
            chart.SecondaryAxis = numericalAxis;
            
            chart.Series.Add(new ColumnSeries()
            {
                Label = "Column Series",
                ItemsSource = MainPage.GetDateTimValue(),
            });
            return chart;
        }
    }
}