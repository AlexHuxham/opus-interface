#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Syncfusion.Drawing;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;
using Syncfusion.Pdf.Parsing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Pdf
{
    class Stamping: ContentPage
    {
        public Stamping()
        {
            Label title = new Label
            {
                Text = "Stamping",
            };

            Label description = new Label
            {
                Text = "\nStamping is often used in documents to ensure that the content has not been modified. Essential PDF provides support for stamping PDF documents with either images or text.\n\n",
            };

            Button button = new Button
            {
                Text = "Generate PDF",
            };
            button.Clicked += OnButtonClicked;
            this.Padding = new Thickness(20, Device.OnPlatform(20, 20, 20), 20, 20);

            if (Device.OS == TargetPlatform.iOS)
            {
                title.Font = Xamarin.Forms.Font.SystemFontOfSize(20);
                description.Font = Xamarin.Forms.Font.SystemFontOfSize(15);
            }
            else if (Device.OS == TargetPlatform.WinPhone)
            {
                title.Font = Xamarin.Forms.Font.SystemFontOfSize(35);
            }
            else if (Device.OS == TargetPlatform.Android)
            {
                title.Font = Xamarin.Forms.Font.SystemFontOfSize(25);
            }
            // Build the page.
            this.Content = new StackLayout
            {
                Children = 
                {
                    title,
                    description,
                    button,
                }
            };
        }
        void OnButtonClicked(object sender, EventArgs e)
        {

            Stream docStream = typeof(App).GetTypeInfo().Assembly.GetManifestResourceStream("Pdf.Assets.Syncfusion_Windows8_whitepaper.pdf");
            PdfLoadedDocument ldoc = new PdfLoadedDocument(docStream);

            PdfFont font = new PdfStandardFont(PdfFontFamily.Helvetica, 100f, PdfFontStyle.Regular);

            foreach (PdfPageBase lPage in ldoc.Pages)
            {
                PdfGraphics g = lPage.Graphics;
                PdfGraphicsState state = g.Save();
                g.SetTransparency(0.25f);
                g.DrawString("Syncfusion", font, PdfPens.Red, PdfBrushes.Red, new PointF(50, lPage.Size.Height / 2 -70));
            }
            MemoryStream stream = new MemoryStream();
            ldoc.Save(stream);
            ldoc.Close(true);
            Xamarin.Forms.DependencyService.Get<ISave>().Save("Stamping.pdf", "application/pdf", stream);
        }
    }
}
