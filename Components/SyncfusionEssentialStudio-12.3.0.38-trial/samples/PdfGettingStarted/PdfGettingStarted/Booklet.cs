#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Syncfusion.Drawing;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Parsing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Reflection;

namespace Pdf
{
    class Booklet : ContentPage
    {
        public Booklet()
        {
            Label title = new Label
            {
                Text = "Booklet",
            };

            Label description = new Label
            {
                Text = "\nThis sample demonstrates Essential PDF's support for creating a booklet from an existing PDF document.\n\n",
            };

            Button button = new Button
            {
                Text = "Generate PDF",
            };
            button.Clicked += OnButtonClicked;
            this.Padding = new Thickness(20, Device.OnPlatform(20, 20, 20), 20, 20);

            if (Device.OS == TargetPlatform.iOS)
            {
                title.Font = Xamarin.Forms.Font.SystemFontOfSize(20);
                description.Font = Xamarin.Forms.Font.SystemFontOfSize(15);
            }
            else if (Device.OS == TargetPlatform.WinPhone)
            {
                title.Font = Xamarin.Forms.Font.SystemFontOfSize(35);
            }
            else if (Device.OS == TargetPlatform.Android)
            {
                title.Font = Xamarin.Forms.Font.SystemFontOfSize(25);
            }
            // Build the page.
            this.Content = new StackLayout
            {
                Children = 
                {
                    title,
                    description,
                    button,
                }
            };
        }
        void OnButtonClicked(object sender, EventArgs e)
        {

            Stream docStream = typeof(App).GetTypeInfo().Assembly.GetManifestResourceStream("Pdf.Assets.Syncfusion_Windows8_whitepaper.pdf");
            PdfLoadedDocument ldoc = new PdfLoadedDocument(docStream);
            float width = 1224;
            float height = 792;

            PdfDocument document = PdfBookletCreator.CreateBooklet(ldoc, new SizeF(width, height), true);
            MemoryStream stream = new MemoryStream();
            document.Save(stream);
            document.Close(true);

            Xamarin.Forms.DependencyService.Get<ISave>().Save("Booklet.pdf", "application/pdf", stream);
        }
    }
}
