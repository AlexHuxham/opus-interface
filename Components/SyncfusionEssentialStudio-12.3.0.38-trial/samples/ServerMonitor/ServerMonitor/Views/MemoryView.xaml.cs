#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Syncfusion.SfChart.XForms;
using Syncfusion.SfGauge.XForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ServerMonitor.Views
{
    #region MemoryView

    public partial class MemoryView
    {
        #region Properties and Fields

        SfCircularGauge circularGauge;

        SfChart chart;

        int i = 40;

        Random rand = new Random();

        #endregion

        #region Methods

        public MemoryView()
        {
            InitializeComponent();
            this.BackgroundColor = Device.OnPlatform(iOS: Color.White, Android: Color.Black, WinPhone: Color.Black);
            this.MemoryLayout.Padding = Device.OnPlatform(iOS: new Thickness(0), Android: new Thickness(50, 0, 50, 0), WinPhone: new Thickness(0));
						this.MemoryChartLayout.Padding = Device.OnPlatform(iOS: new Thickness(0,-100,0,0), Android: new Thickness(0, -135, 0, 0), WinPhone: new Thickness(0,-50,0,0));
            this.MemoryUsage.Font = this.MemoryUsageHistory.Font = Font.OfSize("Normal", 20);
            this.MemoryLayout.Children.Add(getGauge());
            this.MemoryChartLayout.Children.Add(getChart());
        }

        private SfCircularGauge getGauge()
        {
            circularGauge = new SfCircularGauge();            
			circularGauge.BackgroundColor = Device.OnPlatform(iOS: Color.White, Android: Color.Black, WinPhone: Color.Black) ; 
			ObservableCollection<Scale> Scales = new ObservableCollection<Scale>();
            Scale scale = new Scale();
            scale.StartValue = 0;
            scale.EndValue = 100;
            scale.StartAngle = 180;
            scale.SweepAngle = 180;
            scale.RimThickness = 40;
            scale.RimColor = Color.FromRgb(86, 86, 86);
			NeedlePointer pointer = new NeedlePointer();
            pointer.Value = 70;
			pointer.KnobColor = Color.FromHex("#FFE2E2E2");
			pointer.KnobRadius = 20;
			pointer.Type = PointerType.Bar;
			RangePointer rPointer = new RangePointer();
            rPointer.Value = 70;
			rPointer.Color = Color.FromHex("#FF00B2DB");
			rPointer.Thickness = 40;
            pointer.LengthFactor = Device.OnPlatform(1, 1.03, 1);
            pointer.Thickness = 5;
            rPointer.Color = Color.FromRgb(0, 178, 219);
            pointer.Color = Color.FromRgb(226, 226, 226);
            List<Pointer> pointers = new List<Pointer>();
            pointers.Add(rPointer);
            pointers.Add(pointer);
            scale.ShowTicks = false;
            scale.ShowLabels = false;
            scale.Pointers = pointers;
            Scales.Add(scale);
            circularGauge.Scales = Scales;
            return circularGauge;
        }
        
        private SfChart getPieChart()
        {
            SfChart Piechart = new SfChart();
            Piechart.BackgroundColor =Device.OnPlatform(iOS: Color.White, Android: Color.Black, WinPhone: Color.Black) ; 
            Piechart.HeightRequest = Device.OnPlatform(iOS: 250, Android: 250, WinPhone: 200);
            PieSeries pieSeries = new PieSeries();
            ObservableCollection<ChartDataPoint> datas = new ObservableCollection<ChartDataPoint>();
            datas.Add(new ChartDataPoint("A", 54));
            datas.Add(new ChartDataPoint("B", 46));
            pieSeries.ItemsSource = datas;
            pieSeries.Color = Color.FromHex("#FFCA00D4");
            pieSeries.DataMarker.ShowLabel = true;
            pieSeries.Label = "DataPoints";
            Piechart.Series.Add(pieSeries);
            Piechart.BackgroundColor = Color.Black;
            return Piechart;
        }
       
        private SfChart getChart()
        {
            chart = new SfChart();
			chart.BackgroundColor =Device.OnPlatform(iOS: Color.White, Android: Color.Black, WinPhone: Color.Black) ;
            chart.HeightRequest = Device.OnPlatform(iOS:150, Android: 200, WinPhone: 250);
            chart.PrimaryAxis = new NumericalAxis() ;
            chart.SecondaryAxis = new NumericalAxis();
            if (Device.OS == TargetPlatform.Android)
            {
                chart.PrimaryAxis.LabelStyle.TextColor = Color.White;
                chart.SecondaryAxis.LabelStyle.TextColor = Color.White;
            }
            LineSeries lineSeries = new LineSeries();
            ObservableCollection<ChartDataPoint> datas = new ObservableCollection<ChartDataPoint>();
            datas.Add(new ChartDataPoint(1, 59));
            datas.Add(new ChartDataPoint(2, 59));
            datas.Add(new ChartDataPoint(3, 59));
            datas.Add(new ChartDataPoint(4, 59));
            datas.Add(new ChartDataPoint(5, 59));
            datas.Add(new ChartDataPoint(6, 59));
            datas.Add(new ChartDataPoint(7, 60));
            datas.Add(new ChartDataPoint(8, 59));
            datas.Add(new ChartDataPoint(9, 56));
            datas.Add(new ChartDataPoint(10, 56));
            datas.Add(new ChartDataPoint(11, 56));
            datas.Add(new ChartDataPoint(12, 56));
            datas.Add(new ChartDataPoint(13, 56));
            datas.Add(new ChartDataPoint(14, 59));
            datas.Add(new ChartDataPoint(15, 59));
            datas.Add(new ChartDataPoint(16, 59));
            datas.Add(new ChartDataPoint(17, 59));
            datas.Add(new ChartDataPoint(18, 59));
            datas.Add(new ChartDataPoint(19, 59));
            datas.Add(new ChartDataPoint(20, 59));
            datas.Add(new ChartDataPoint(21, 59));
            datas.Add(new ChartDataPoint(22, 59));
            datas.Add(new ChartDataPoint(23, 59));
            datas.Add(new ChartDataPoint(24, 59));
            datas.Add(new ChartDataPoint(25, 58));
            datas.Add(new ChartDataPoint(26, 58));
            datas.Add(new ChartDataPoint(27, 58));
            datas.Add(new ChartDataPoint(28, 58));
            datas.Add(new ChartDataPoint(29, 58));
            datas.Add(new ChartDataPoint(30, 59));
            datas.Add(new ChartDataPoint(31, 59));
            datas.Add(new ChartDataPoint(32, 59));
            datas.Add(new ChartDataPoint(33, 59));
            datas.Add(new ChartDataPoint(34, 55));
            datas.Add(new ChartDataPoint(35, 55));
            datas.Add(new ChartDataPoint(36, 55));
            datas.Add(new ChartDataPoint(37, 55));
            datas.Add(new ChartDataPoint(38, 59));
            datas.Add(new ChartDataPoint(39, 62));
            datas.Add(new ChartDataPoint(40, 62));
            lineSeries.ItemsSource = datas;
            //lineSeries.DataMarker.ShowLabel = true;
            //lineSeries.DataMarker.LabelStyle.TextColor = Color.White;
            lineSeries.Label = "DataPoints";
            lineSeries.Color = Color.Purple;
            chart.Series.Add(lineSeries);
            Device.StartTimer(new TimeSpan(0, 0, 0, 1), AddData);
            return chart;
        }
        
        private bool AddData()
        {
            var MemoryValue = DataGenerator.MemoryUnits;
            i = i + 1;
			if (Device.OS == TargetPlatform.iOS) {
				circularGauge.GaugeValue = DataGenerator.MemoryUnits;
			} else {
            circularGauge.Scales[0].Pointers[0].Value = DataGenerator.MemoryUnits;
            circularGauge.Scales[0].Pointers[1].Value = DataGenerator.MemoryUnits;
			}
            (chart.Series[0].ItemsSource as ObservableCollection<ChartDataPoint>).RemoveAt(0);
            (chart.Series[0].ItemsSource as ObservableCollection<ChartDataPoint>).Add(new ChartDataPoint(i, MemoryValue));
            return true;
        }

        #endregion

    }

    #endregion
}
