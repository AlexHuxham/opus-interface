#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Syncfusion.SfChart.XForms;
using Syncfusion.SfGauge.XForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ServerMonitor.Views
{
    #region CPUView

    public partial class CPUView
    {

        # region Properties and Fields

        SfChart chart;

        SfCircularGauge circularGauge;

        int i = 40;

        Random rand = new Random();

        # endregion

        # region Methods

        public CPUView()
        {
            InitializeComponent();            
            this.CPUUtilization.Font = this.CPUUtilizationHistory.Font = Font.OfSize("Normal",20);
            this.BackgroundColor=this.CPUUtilizationHistory.BackgroundColor = this.CPUUtilization.BackgroundColor =this.CPUChartLayout.BackgroundColor= this.CPULayout.BackgroundColor = Device.OnPlatform(iOS: Color.White, Android: Color.Black, WinPhone: Color.Black);
            this.CPULayout.Padding = Device.OnPlatform(iOS: new Thickness(0), Android: new Thickness(50, 0, 50, 0), WinPhone: new Thickness(0));
            this.CPUChartLayout.Padding = Device.OnPlatform(iOS: new Thickness(0,-100,0,0), Android: new Thickness(0,-135,0,0), WinPhone: new Thickness(0,-50,0,0));
            StackLayout view = new StackLayout();
            view.IsClippedToBounds = true;
            view.Children.Add(getGauge());
            view.HeightRequest = 250;
            this.CPULayout.Children.Add(view);
            this.CPUChartLayout.Children.Add(getChart());
			Device.StartTimer(new TimeSpan(0, 0, 0,1), AddData);
        }
                
        private SfChart getChart()
        {
            chart = new SfChart();
			chart.BackgroundColor =Device.OnPlatform(iOS: Color.White, Android: Color.Black, WinPhone: Color.Black) ;
            chart.HeightRequest = Device.OnPlatform(iOS:150, Android: 200, WinPhone: 250) ;
            chart.WidthRequest = 250;
            chart.PrimaryAxis = new NumericalAxis() ;
            chart.SecondaryAxis = new NumericalAxis() { EdgeLabelsDrawingMode = EdgeLabelsDrawingMode.Shift };
            if (Device.OS == TargetPlatform.Android)
            {
                chart.PrimaryAxis.LabelStyle.TextColor = Color.White;
                chart.SecondaryAxis.LabelStyle.TextColor = Color.White;
            }
            LineSeries lineSeries = new LineSeries();
            ObservableCollection<ChartDataPoint> datas = new ObservableCollection<ChartDataPoint>();
            datas.Add(new ChartDataPoint(1, 34));
            datas.Add(new ChartDataPoint(2, 24));
            datas.Add(new ChartDataPoint(3, 19));
            datas.Add(new ChartDataPoint(4, 21));
            datas.Add(new ChartDataPoint(5, 25));
            datas.Add(new ChartDataPoint(6, 5));
            datas.Add(new ChartDataPoint(7, 34));
            datas.Add(new ChartDataPoint(8, 24));
            datas.Add(new ChartDataPoint(9, 19));
            datas.Add(new ChartDataPoint(10, 21));
            datas.Add(new ChartDataPoint(11, 25));
            datas.Add(new ChartDataPoint(12, 76));
            datas.Add(new ChartDataPoint(13, 34));
            datas.Add(new ChartDataPoint(14, 24));
            datas.Add(new ChartDataPoint(15, 19));
            datas.Add(new ChartDataPoint(16, 21));
            datas.Add(new ChartDataPoint(17, 25));
            datas.Add(new ChartDataPoint(18, 32));
            datas.Add(new ChartDataPoint(19, 0));
            datas.Add(new ChartDataPoint(20, 32));
            datas.Add(new ChartDataPoint(21, 25));
            datas.Add(new ChartDataPoint(22, 32));
            datas.Add(new ChartDataPoint(23, 34));
            datas.Add(new ChartDataPoint(24, 24));
            datas.Add(new ChartDataPoint(25, 19));
            datas.Add(new ChartDataPoint(26, 21));
            datas.Add(new ChartDataPoint(27, 25));
            datas.Add(new ChartDataPoint(28, 32));
            datas.Add(new ChartDataPoint(29, 25));
            datas.Add(new ChartDataPoint(30, 32));
            datas.Add(new ChartDataPoint(31, 74));
            datas.Add(new ChartDataPoint(32, 32));
            datas.Add(new ChartDataPoint(33, 34));
            datas.Add(new ChartDataPoint(34, 24));
            datas.Add(new ChartDataPoint(35, 19));
            datas.Add(new ChartDataPoint(36, 21));
            datas.Add(new ChartDataPoint(37, 25));
            datas.Add(new ChartDataPoint(38, 32));
            datas.Add(new ChartDataPoint(39, 25));
            datas.Add(new ChartDataPoint(40, 32));
            lineSeries.ItemsSource = datas;
            //lineSeries.DataMarker.ShowLabel = false;
            //lineSeries.DataMarker.LabelStyle.TextColor = Color.White;
            lineSeries.Label = "DataPoints";
            chart.Series.Add(lineSeries);
            Device.StartTimer(new TimeSpan(0, 0, 0,1), AddData);
            return chart;
        }
        
        private SfCircularGauge getGauge()
		{
			circularGauge = new SfCircularGauge ();
			circularGauge.BackgroundColor = Device.OnPlatform (iOS: Color.White, Android: Color.Black, WinPhone: Color.Black);
			ObservableCollection<Scale> Scales = new ObservableCollection<Scale> ();
			Scale scale = new Scale ();
			scale.StartValue = 0;
			scale.EndValue = 100;
			scale.StartAngle = 180;
			scale.SweepAngle = 180;
			scale.RimThickness = 40;
            scale.RimColor = Color.FromRgb(86, 86, 86);
			NeedlePointer pointer = new NeedlePointer ();
			pointer.Value = 70;
			pointer.Thickness = 5;
			pointer.KnobRadius = 20;
            pointer.LengthFactor  = Device.OnPlatform(1,1.03,1);
            pointer.Color = Color.FromRgb(226, 226, 226);
            pointer.KnobColor = Color.FromRgb(226, 226, 226);
			RangePointer rPointer = new RangePointer ();
			rPointer.Value = 70;
			rPointer.Color = Color.FromHex ("#FFF47F30");
			rPointer.Thickness = 40;
			List<Pointer> pointers = new List<Pointer> ();
			pointers.Add (rPointer);
            pointers.Add(pointer);
            scale.Pointers = pointers;
            scale.ShowTicks = false;
            scale.ShowLabels = false;
            Scales.Add(scale);
            circularGauge.Scales = Scales;
            return circularGauge;
        }
        
        private bool AddData()
        {             
            i = i + 1;
			i = i + 1;
			if (Device.OS == TargetPlatform.iOS) {
				circularGauge.GaugeValue = DataGenerator.CPUUnits;

			} else {
				circularGauge.Scales[0].Pointers[0].Value = DataGenerator.CPUUnits;
				circularGauge.Scales[0].Pointers[1].Value = DataGenerator.CPUUnits;
			}
            (chart.Series[0].ItemsSource as ObservableCollection<ChartDataPoint>).RemoveAt(0);
            (chart.Series[0].ItemsSource as ObservableCollection<ChartDataPoint>).Add(new ChartDataPoint(i, DataGenerator.CPUUnits));
            return true;
        }

        #endregion

    }

    #endregion
}
