#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Syncfusion.SfChart.XForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ServerMonitor.Views
{
    #region NetworkView

    public partial class NetworkView
    {
        #region Properties and fields

        SfChart chart;
        int i = 40;
        Random rand = new Random();

        #endregion

        #region Methods

        public NetworkView()
        {
            InitializeComponent();

			this.BackgroundColor = Device.OnPlatform(iOS: Color.White, Android: Color.Black, WinPhone: Color.Black);
			this.DataDownload.Font = this.MemoryUsed.Font = Font.OfSize("Normal", 15);
            this.NetworkLayout.Padding = Device.OnPlatform(iOS: new Thickness(0), Android: new Thickness(0), WinPhone: new Thickness(0, 0, 0, 0));

            this.MemoryPieLayout.Padding = Device.OnPlatform(iOS: new Thickness(0), Android: new Thickness(0), WinPhone: new Thickness(0, -5, 0, 0)); 
            this.MemoryPieLayout.HeightRequest = Device.OnPlatform(iOS: 225, Android: 225, WinPhone: 275);
			dummyLayout.HeightRequest = Device.OnPlatform (iOS:15,Android:0,WinPhone:0);
			lowerLayout.HeightRequest = Device.OnPlatform (iOS:0,Android:0,WinPhone:0);
			LowerChartLayout.HeightRequest = Device.OnPlatform (iOS:0,Android:0,WinPhone:15);
			LowerChartLayout.WidthRequest = 50;
			dummyLayout1.HeightRequest = Device.OnPlatform (iOS:0,Android:0,WinPhone:0);
            this.NetworkLayout.Children.Add(getChart());
            this.MemoryPieLayout.Children.Add(getPieChart());
         }

        private SfChart getChart()
        {
            chart = new SfChart();
			chart.BackgroundColor  =Device.OnPlatform(iOS: Color.White, Android: Color.Black, WinPhone: Color.Black) ;
            chart.HeightRequest = Device.OnPlatform(iOS: 150, Android: 200, WinPhone: 280);
			chart.PrimaryAxis = new NumericalAxis();
             chart.SecondaryAxis = new NumericalAxis();
             if (Device.OS == TargetPlatform.Android)
             {
                 chart.PrimaryAxis.LabelStyle.TextColor = Color.White;
                 chart.SecondaryAxis.LabelStyle.TextColor = Color.White;
             }
            LineSeries lineSeries = new LineSeries();
            if (Device.OS != TargetPlatform.iOS)
                lineSeries.Color = Color.FromHex("#FFB37300");
            ObservableCollection<ChartDataPoint> datas = new ObservableCollection<ChartDataPoint>();
            datas.Add(new ChartDataPoint(1, 8));
            datas.Add(new ChartDataPoint(2, 8));
            datas.Add(new ChartDataPoint(3, 64));
            datas.Add(new ChartDataPoint(4, 64));
            datas.Add(new ChartDataPoint(5, 256));
            datas.Add(new ChartDataPoint(6, 10));
            datas.Add(new ChartDataPoint(7, 10));
            datas.Add(new ChartDataPoint(8, 64));
            datas.Add(new ChartDataPoint(9, 256));
            datas.Add(new ChartDataPoint(10, 8));
            datas.Add(new ChartDataPoint(11, 8));
            datas.Add(new ChartDataPoint(12, 8));
            datas.Add(new ChartDataPoint(13, 64));
            datas.Add(new ChartDataPoint(14, 64));
            datas.Add(new ChartDataPoint(15, 256));
            datas.Add(new ChartDataPoint(16, 20));
            datas.Add(new ChartDataPoint(17, 20));
            datas.Add(new ChartDataPoint(18, 64));
            datas.Add(new ChartDataPoint(19, 256));
            datas.Add(new ChartDataPoint(20, 8));
            datas.Add(new ChartDataPoint(21, 8));
            datas.Add(new ChartDataPoint(22, 8));
            datas.Add(new ChartDataPoint(23, 64));
            datas.Add(new ChartDataPoint(24, 64));
            datas.Add(new ChartDataPoint(25, 256));
            datas.Add(new ChartDataPoint(26, 10));
            datas.Add(new ChartDataPoint(27, 10));
            datas.Add(new ChartDataPoint(28, 64));
            datas.Add(new ChartDataPoint(29, 256));
            datas.Add(new ChartDataPoint(30, 18));
            datas.Add(new ChartDataPoint(31, 15));
            datas.Add(new ChartDataPoint(32, 25));
            datas.Add(new ChartDataPoint(33, 64));
            datas.Add(new ChartDataPoint(34, 64));
            datas.Add(new ChartDataPoint(35, 256));
            datas.Add(new ChartDataPoint(36, 30));
            datas.Add(new ChartDataPoint(37, 20));
            datas.Add(new ChartDataPoint(38, 64));
            datas.Add(new ChartDataPoint(39, 256));
            datas.Add(new ChartDataPoint(40, 18));
            lineSeries.ItemsSource = datas;
        
            chart.Series.Add(lineSeries);
            Device.StartTimer(new TimeSpan(0, 0, 1), AddData);
            return chart;
        }

        private SfChart getPieChart()
        {
            SfChart Piechart = new SfChart();
            Piechart.BackgroundColor = Device.OnPlatform(iOS: Color.White, Android: Color.Black, WinPhone: Color.Black);
            Piechart.HeightRequest = Device.OnPlatform(iOS: 200, Android: 200, WinPhone: 250);
            PieSeries pieSeries = new PieSeries();
            ObservableCollection<ChartDataPoint> datas = new ObservableCollection<ChartDataPoint>();
            datas.Add(new ChartDataPoint("54% Used", 54));
            datas.Add(new ChartDataPoint("46% Free", 46));
            pieSeries.ItemsSource = datas;
            pieSeries.DataMarker = new ChartDataMarker();
            pieSeries.DataMarker.ShowLabel = true;
            Piechart.Series.Add(pieSeries);
            Piechart.Legend = new ChartLegend() { IsVisible = true };
			if (Device.OS != TargetPlatform.iOS) {
				Piechart.Legend.LabelStyle.TextColor = Color.White;
				Piechart.Legend.LabelStyle.Font = Font.OfSize (null, 12);
			}
            return Piechart;
        }

        private bool AddData()
        {
            i = i + 1;
            (chart.Series[0].ItemsSource as ObservableCollection<ChartDataPoint>).RemoveAt(0);
            (chart.Series[0].ItemsSource as ObservableCollection<ChartDataPoint>).Add(new ChartDataPoint(i,DataGenerator.DownLoadRate));
            return true;
        }

        #endregion

    }

    #endregion
}