#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using System.Reflection;
using Xamarin.Forms.Platform.iOS;


using MonoTouch.UIKit;
using MonoTouch.Foundation;
using Native = Syncfusion.SfGauge.iOS.SFRadialGauge;



[assembly: ExportRenderer(typeof(Syncfusion.SfGauge.XForms.SfCircularGauge), typeof(Syncfusion.SfGauge.XForms.iOS.SfGaugeRenderer))]

namespace Syncfusion.SfGauge.XForms.iOS
{

	public class SfGaugeRenderer : ViewRenderer<Syncfusion.SfGauge.XForms.SfCircularGauge, Native>
	{
		Syncfusion.SfGauge.XForms.SfCircularGauge gauge;
		Native nativeGauge;
        SfCircularGaugeMapping SfCircularGaugeMapping=  new SfCircularGaugeMapping();
		public SfGaugeRenderer()
		{
			SfCircularGaugeMapping = new SfCircularGaugeMapping ();
		}

		protected override void OnElementChanged(ElementChangedEventArgs<Syncfusion.SfGauge.XForms.SfCircularGauge> e)
		{
			base.OnElementChanged(e);

			gauge = e.NewElement;
			nativeGauge = new Native();

			List<string> propertiesNames = GetPropertiesChanged(typeof(SfCircularGauge), gauge);

			foreach (var name in propertiesNames)
			{
				SfCircularGaugeMapping.OnGaugePropertiesChanged(name, gauge, nativeGauge);
			}

			SetNativeControl(nativeGauge);
			propertiesNames.Clear();
		}

		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{

			if (sender is SfCircularGauge)
			{
				SfCircularGaugeMapping.OnGaugePropertiesChanged(e.PropertyName, gauge, nativeGauge);
			}
		}

		public static List<string> GetPropertiesChanged(Type type, Object obj)
		{
			var list = type.GetProperties();
			List<string> proplist = new List<string>();
			foreach (var item in list)
			{
				proplist.Add(item.Name);
			}
			return proplist;
			
		}
	}
}