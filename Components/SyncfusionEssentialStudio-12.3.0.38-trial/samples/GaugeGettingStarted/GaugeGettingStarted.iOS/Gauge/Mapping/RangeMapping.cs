#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MonoTouch.UIKit;
using MonoTouch.Foundation;
using Native = Syncfusion.SfGauge.iOS.SFRange;
using Xamarin.Forms;
using Syncfusion.SfGauge.XForms;

namespace Syncfusion.SfGauge.XForms.iOS
{
    public class RangeMapping
    {
        public Native GetRange(Range formRange)
        {
            Native newRange = new Native();
            if (formRange is Range)
            {
                List<string> properties = SfGaugeRenderer.GetPropertiesChanged(typeof(Range), formRange);
                foreach (var propertyName in properties)
                {
                    OnRangePropertiesChanged(propertyName, (Range)formRange, (Native)newRange);
                }
            }

            return newRange;
        }

        public void OnRangePropertiesChanged(string propertyName, Range formRange, Native nativeRange)
        {
            if (propertyName == Range.StartValueProperty.PropertyName)
            {
                nativeRange.StartValue = float.Parse(formRange.StartValue.ToString());
            }

            if (propertyName == Range.EndValueProperty.PropertyName)
            {
                nativeRange.EndValue = float.Parse(formRange.EndValue.ToString());
            }

			if (propertyName == Range.ColorProperty.PropertyName)
            {
				nativeRange.Color = UIColor.FromRGB(float.Parse(formRange.Color.R.ToString()), float.Parse(formRange.Color.G.ToString()), float.Parse(formRange.Color.B.ToString()));
            }

			if (propertyName == Range.ThicknessProperty.PropertyName)
            {
				nativeRange.Thinkness = float.Parse(formRange.Thickness.ToString());
//                nativeRange.Offset = 2;
            }
        }

    }
}