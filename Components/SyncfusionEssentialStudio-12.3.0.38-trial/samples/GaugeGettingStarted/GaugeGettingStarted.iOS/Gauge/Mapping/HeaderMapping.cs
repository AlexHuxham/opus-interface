#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MonoTouch.UIKit;
using MonoTouch.Foundation;
using Native = Syncfusion.SfGauge.iOS.SFGaugeHeader;
using Xamarin.Forms;
using Syncfusion.SfGauge.XForms;
using System.Drawing;

namespace Syncfusion.SfGauge.XForms.iOS
{
    public static class HeaderMapping
    {

		public static Native GetHeader(Header gaugeHeader)
        {
            Native newHeader = new Native();
            if (gaugeHeader is Header)
            {
                List<string> properties = SfGaugeRenderer.GetPropertiesChanged(typeof(Header), gaugeHeader);
                foreach (var propertyName in properties)
                {
                    OnGaugeHeaderPropertiesChanged(propertyName, (Header)gaugeHeader, (Native)newHeader);
                }
            }

            return newHeader;
        }

        public static void OnGaugeHeaderPropertiesChanged(string propertyName, Header formHeader, Native nativeHeader)
        {
			if (propertyName == Header.TextProperty.PropertyName)
            {
				nativeHeader.Text = formHeader.Text;
            }
			if (propertyName == Header.PositionProperty.PropertyName)
			{
				nativeHeader.Position = new PointF( (float)formHeader.Position.X,(float)formHeader.Position.Y);
			}
			if (propertyName == Header.ForegroundColorProperty.PropertyName)
			{
				nativeHeader.ForegroundColor = UIColor.FromRGB(float.Parse(formHeader.ForegroundColor.R.ToString()),float.Parse(formHeader.ForegroundColor.G.ToString()),float.Parse(formHeader.ForegroundColor.B.ToString()));
			}
			if (propertyName == Header.FontProperty.PropertyName)
			{
//				nativeHeader.Font = formHeader.Font;
			}
        }


    }
}