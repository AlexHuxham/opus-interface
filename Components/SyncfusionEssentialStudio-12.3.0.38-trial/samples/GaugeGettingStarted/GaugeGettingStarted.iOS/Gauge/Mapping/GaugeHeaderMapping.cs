#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MonoTouch.UIKit;
using MonoTouch.Foundation;
using Native = Syncfusion.SfGauge.iOS.SFGaugeHeader;
using Xamarin.Forms;
using Syncfusion.XForms.SfGauge.SfCircularGauge;

namespace Syncfusion.XForms.SfGauge.iOS.Mapping
{
    public static class GaugeHeaderMapping
    {

        public static Native GetHeader(SfCircularGauge.GaugeHeader gaugeHeader)
        {
            Native newHeader = new Native();
            if (gaugeHeader is SfCircularGauge.GaugeHeader)
            {
                List<string> properties = SfGaugeRenderer.GetPropertiesChanged(typeof(SfCircularGauge.GaugeHeader), gaugeHeader);
                foreach (var propertyName in properties)
                {
                    OnGaugeHeaderPropertiesChanged(propertyName, (SfCircularGauge.GaugeHeader)gaugeHeader, (Native)newHeader);
                }
            }

            return newHeader;
        }

        public static void OnGaugeHeaderPropertiesChanged(string propertyName, SfCircularGauge.GaugeHeader formHeader, Native nativeHeader)
        {
            if (propertyName == GaugeHeader.HeaderTextProperty.PropertyName)
            {
				nativeHeader.Text = formHeader.HeaderText;
            }
        }


    }
}