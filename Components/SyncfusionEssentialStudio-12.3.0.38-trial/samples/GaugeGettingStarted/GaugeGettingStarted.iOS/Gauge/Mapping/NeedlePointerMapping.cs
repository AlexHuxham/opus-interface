#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mon= MonoTouch.ObjCRuntime.Runtime;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using Xamarin.Forms;
using Syncfusion.SfGauge.XForms;
using Native = Syncfusion.SfGauge.iOS.SFNeedlePointer;
using Syncfusion.SfGauge.iOS;



namespace Syncfusion.SfGauge.XForms.iOS
{
	public class NeedlePointerMapping:PointerMapping
	{
//		public Syncfusion.SfGauge.iOS.SFRadialGauge gauge;
		public  Native GetNeedlePointer(NeedlePointer formPointer)
		{

			Native newPointer = new Native();
			if (formPointer is NeedlePointer)
			{
				List<string> properties = SfGaugeRenderer.GetPropertiesChanged(typeof(NeedlePointer), formPointer);
				foreach (var propertyName in properties)
				{
					OnPointerPropertiesChanged(propertyName, (NeedlePointer)formPointer, (Native)newPointer);
				}
			}
			return newPointer;
		}

		public  void OnPointerPropertiesChanged (string propertyName, NeedlePointer formPointer, Native nativePointer)
		{
			if (propertyName == NeedlePointer.ValueProperty.PropertyName) 
			{
				nativePointer.Value = float.Parse (formPointer.Value.ToString ());

			}

			if (propertyName == NeedlePointer.ColorProperty.PropertyName) {
				nativePointer.Color = UIColor.FromRGB (float.Parse (formPointer.Color.R.ToString ()), float.Parse (formPointer.Color.G.ToString ()), float.Parse (formPointer.Color.B.ToString ()));
			}

			if (propertyName == NeedlePointer.KnobColorProperty.PropertyName) {
				nativePointer.KnobColor = UIColor.FromRGB ((float)formPointer.KnobColor.R, (float)formPointer.KnobColor.G, (float)formPointer.KnobColor.B);
			}

			if (propertyName == NeedlePointer.LengthFactorProperty.PropertyName) {
				nativePointer.LengthFactor = (float)(formPointer.LengthFactor / 2.7);
			}

			if (propertyName == NeedlePointer.ThicknessProperty.PropertyName) {
				nativePointer.Thinkness = (float)(formPointer.Thickness / 1000);
			}

			if (propertyName == NeedlePointer.KnobRadiusProperty.PropertyName) {
				nativePointer.KnobRadius = (float)(formPointer.KnobRadius / 1000);
			}

			if (propertyName == NeedlePointer.TypeProperty.PropertyName) {
				if (formPointer.Type == Syncfusion.SfGauge.XForms.PointerType.Bar)
					nativePointer.PointerType = Syncfusion.SfGauge.iOS.PointerType.Bar;
				else
					nativePointer.PointerType = Syncfusion.SfGauge.iOS.PointerType.Triangle;
			}
		}
	}
}
	


