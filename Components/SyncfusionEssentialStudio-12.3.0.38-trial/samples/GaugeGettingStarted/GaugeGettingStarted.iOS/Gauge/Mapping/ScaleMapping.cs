#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MonoTouch.UIKit;
using MonoTouch.Foundation;
using Native = Syncfusion.SfGauge.iOS.SFScale;
using native = Syncfusion.SfGauge.iOS;
using Xamarin.Forms;
using Syncfusion.SfGauge.XForms;

namespace Syncfusion.SfGauge.XForms.iOS
{
    public class ScaleMapping
    {
        public native.SFRadialGauge gauge { get; set; }
        public SfGaugeRenderer renderer;
        public Native GetScale(Scale formScale)
        {
            Native newScale = new Native();

            if (formScale is Scale)
            {
                List<string> properties = SfGaugeRenderer.GetPropertiesChanged(typeof(Scale), formScale);
                foreach (var propertyName in properties)
                {
                    OnScalePropertiesChanged(propertyName, (Scale)formScale, (Native)newScale);
                }
            }

            return newScale;
        }
        PointerMapping pointerMapping = new PointerMapping();
        public void OnScalePropertiesChanged(string propertyName, Scale formScale, Native nativeScale)
        {
            if (propertyName == Scale.StartAngleProperty.PropertyName)
            {
                nativeScale.StartAngle = float.Parse((formScale.StartAngle - 90).ToString());
            }
            if (propertyName == Scale.SweepAngleProperty.PropertyName)
            {
                nativeScale.SweepAngle = float.Parse((formScale.SweepAngle + 45).ToString());
            }

            if (propertyName == Scale.StartValueProperty.PropertyName)
            {
                nativeScale.StartValue = float.Parse(formScale.StartValue.ToString());
            }

            if (propertyName == Scale.EndValueProperty.PropertyName)
            {
                nativeScale.EndValue = float.Parse(formScale.EndValue.ToString());
            }

            if (propertyName == Scale.RimThicknessProperty.PropertyName)
            {
                nativeScale.RimThinkness = float.Parse((formScale.RimThickness / 400.0).ToString());
            }

            if (propertyName == Scale.LabelColorProperty.PropertyName)
            {
                nativeScale.LabelColor = UIColor.FromRGB((float)formScale.LabelColor.R, (float)formScale.LabelColor.G, (float)formScale.LabelColor.B);
            }

            if (propertyName == Scale.RimColorProperty.PropertyName)
            {
                nativeScale.RimColor = UIColor.FromRGB(float.Parse(formScale.RimColor.R.ToString()), float.Parse(formScale.RimColor.G.ToString()), float.Parse(formScale.RimColor.B.ToString()));
            }
				
            if (propertyName == Scale.MinorTicksPerIntervalProperty.PropertyName)
            {
				nativeScale.MinorTicksPerInterval = float.Parse(formScale.MinorTicksPerInterval.ToString());
            }
            if (propertyName == Scale.IntervalProperty.PropertyName)
            {
				nativeScale.Interval = float.Parse( formScale.Interval.ToString());
				nativeScale.Radious = 150f;
            }

            if (propertyName == Scale.PointersProperty.PropertyName)
            {
                nativeScale.Pointers = new NSMutableArray();
                if (formScale.Pointers != null)
                {
					NeedlePointerMapping npm;
                    foreach (Pointer formPointer in formScale.Pointers)
                    {
						npm = new NeedlePointerMapping ();
						if (formPointer is NeedlePointer) {
							pointerMapping.gauge = this.gauge;
							var newPointer = npm.GetNeedlePointer ((NeedlePointer)formPointer);
							nativeScale.Pointers.Add (newPointer);
						}
                    }
					RangePointerMapping rpm;
					foreach (Pointer formPointer in formScale.Pointers)
					{
						rpm = new RangePointerMapping ();
						if (formPointer is RangePointer) {
							pointerMapping.gauge = this.gauge;
							var newPointer = rpm.GetRangePointer ((RangePointer)formPointer);
							nativeScale.Pointers.Add (newPointer);
						}
					}
                }
            }


            if (propertyName == Scale.RangesProperty.PropertyName)
            {
                nativeScale.Ranges = new NSMutableArray();
                if (formScale.Ranges != null)
                {
                    RangeMapping rangeMapping;
                    foreach (Range formRange in formScale.Ranges)
                    {
                        rangeMapping = new RangeMapping();
                        var newRange = rangeMapping.GetRange(formRange);
                        nativeScale.Ranges.Add(newRange);
                    }
                }
            }

			if (propertyName == Scale.MinorTickSettingsProperty.PropertyName)
			{
				if (formScale.MinorTickSettings != null)
				{
					TickSettingMapping ticksMapping;
					foreach (TickSettings formTicks in formScale.MinorTickSettings)
					{
						ticksMapping = new TickSettingMapping();
						var newticks = ticksMapping.GetTicks(formTicks);
						nativeScale.MinorTickSettings = newticks;
					}
				}
			}

			if (propertyName == Scale.MajorTickSettingsProperty.PropertyName)
			{
				if (formScale.MajorTickSettings != null)
				{
					TickSettingMapping ticksMapping;
					foreach (TickSettings formTicks in formScale.MajorTickSettings)
					{
						ticksMapping = new TickSettingMapping();
						var newticks = ticksMapping.GetTicks(formTicks);
						nativeScale.MajorTickSettings = newticks;
					}
				}
			}
        }
       

    }
}