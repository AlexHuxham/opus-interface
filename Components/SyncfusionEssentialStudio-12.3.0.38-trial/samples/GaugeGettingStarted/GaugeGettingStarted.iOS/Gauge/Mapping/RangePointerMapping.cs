#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mon= MonoTouch.ObjCRuntime.Runtime;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using Xamarin.Forms;
using Syncfusion.SfGauge.XForms;
using Native = Syncfusion.SfGauge.iOS.SFRangePointer;
using Syncfusion.SfGauge.iOS;
namespace Syncfusion.SfGauge.XForms.iOS
{
	public class RangePointerMapping:PointerMapping
	{
		public Syncfusion.SfGauge.iOS.SFRadialGauge gauge;
		public  Native GetRangePointer(Pointer formPointer)
		{

			Native newPointer = new Native();
			if (formPointer is Pointer)
			{
				List<string> properties = SfGaugeRenderer.GetPropertiesChanged(typeof(Pointer), formPointer);
				foreach (var propertyName in properties)
				{
					OnPointerPropertiesChanged(propertyName, (Pointer)formPointer, (Native)newPointer);
				}
			}
			return newPointer;
		}

		public  void OnPointerPropertiesChanged(string propertyName, Pointer formPointer, Native nativePointer)
		{
			if (propertyName == Pointer.ValueProperty.PropertyName)
			{
				nativePointer.Value = float.Parse(formPointer.Value.ToString());

			}

			if (propertyName == Pointer.ColorProperty.PropertyName)
			{
				nativePointer.Color = UIColor.FromRGB(float.Parse(formPointer.Color.R.ToString()),float.Parse(formPointer.Color.G.ToString()),float.Parse(formPointer.Color.B.ToString()));
			}

			if (propertyName == Pointer.ThicknessProperty.PropertyName)
			{
				nativePointer.Thinkness = (float)(formPointer.Thickness/1000);
			}

			if (propertyName == Pointer.EnableAnimationProperty.PropertyName)
			{
				nativePointer.EnableAnimation = formPointer.EnableAnimation;
			}


		}
	}
}

