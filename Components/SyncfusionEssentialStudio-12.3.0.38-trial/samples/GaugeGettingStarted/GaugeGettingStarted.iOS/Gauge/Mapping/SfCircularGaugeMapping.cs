#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MonoTouch.UIKit;
using MonoTouch.Foundation;
using Native = Syncfusion.SfGauge.iOS.SFRadialGauge;
using Xamarin.Forms;
using System.Drawing;
using Syncfusion.SfGauge.XForms;
namespace Syncfusion.SfGauge.XForms.iOS
{
    public class SfCircularGaugeMapping
    {
        ScaleMapping ScaleMapping = new ScaleMapping();
        int i = 0;
        public void OnGaugePropertiesChanged(string propertyName, SfCircularGauge formsGauge, Native nativeGauge)
        {
//            if (propertyName == SfCircularGauge.SfCircularGauge.GaugeHeightProperty.PropertyName)
//            {
//				nativeGauge.Frame = new RectangleF(nativeGauge.Frame.X, nativeGauge.Frame.Y, nativeGauge.Frame.Width, float.Parse((formsGauge.GaugeHeight-200).ToString()));
//            }
//
//            if (propertyName == SfCircularGauge.SfCircularGauge.GaugeWidthProperty.PropertyName)
//            {
//				nativeGauge.Frame = new RectangleF(nativeGauge.Frame.X, nativeGauge.Frame.Y, nativeGauge.Frame.Width, float.Parse((formsGauge.GaugeWidth-200).ToString()));
//            }

			if (propertyName == SfCircularGauge.FrameBackgroundColorProperty.PropertyName)
            {
				nativeGauge.BackgroundColor = UIColor.FromRGB(float.Parse(formsGauge.FrameBackgroundColor.R.ToString()), float.Parse(formsGauge.FrameBackgroundColor.G.ToString()), float.Parse(formsGauge.FrameBackgroundColor.B.ToString()));
            }

//            if (propertyName == SfCircularGauge.SfCircularGauge.GaugeValueProperty.PropertyName)
//            {
//                if(i!=0)
//                nativeGauge.PointerValue = float.Parse(formsGauge.GaugeValue.ToString());
//                i++;
//            }

            if (propertyName == SfCircularGauge.GaugeHeadersProperty.PropertyName)
            {
				if (formsGauge.Headers != null)
                {
                    NSMutableArray nativeheader = new NSMutableArray();
					foreach (Header FormHeader in formsGauge.Headers)
                    {
						var newHeader = HeaderMapping.GetHeader(FormHeader);
                        nativeheader.Add(newHeader);
                    }
                    nativeGauge.Headers = nativeheader;

                }
            }


            if (propertyName == SfCircularGauge.ScalesProperty.PropertyName)
            {
                if (formsGauge.Scales != null)
                {
                    NSMutableArray scale = new NSMutableArray();
                    foreach (Scale FormScale in formsGauge.Scales)
                    {
                        var newScale = ScaleMapping.GetScale(FormScale);
                        scale.Add(newScale);
                    }
                    nativeGauge.Scales = scale;

                }
            }


        }

    }
}