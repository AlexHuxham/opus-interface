#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mon= MonoTouch.ObjCRuntime.Runtime;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using Xamarin.Forms;
using Syncfusion.SfGauge.XForms;
using Native = Syncfusion.SfGauge.iOS.SFTickSettings;
using Syncfusion.SfGauge.iOS;

namespace Syncfusion.SfGauge.XForms.iOS
{
	public class TickSettingMapping
	{
		public TickSettingMapping ()
		{
		}
		public  Native GetTicks(TickSettings formTicks)
		{

			Native newTicks = new Native();
			if (formTicks is TickSettings)
			{
				List<string> properties = SfGaugeRenderer.GetPropertiesChanged(typeof(TickSettings), formTicks);
				foreach (var propertyName in properties)
				{
					OnPointerPropertiesChanged(propertyName, (TickSettings)formTicks, (Native)newTicks);
				}
			}
			return newTicks;
		}

		public  void OnPointerPropertiesChanged(string propertyName, TickSettings formTicks, Native nativeTicks)
		{
			if (propertyName == TickSettings.LengthProperty.PropertyName)
			{
				nativeTicks.Length =  float.Parse(((formTicks.Length / 100) / 3).ToString());
			}

			if (propertyName ==TickSettings.ColorProperty.PropertyName)
			{
				nativeTicks.Color =  UIColor.FromRGB(float.Parse(formTicks.Color.R.ToString()),float.Parse(formTicks.Color.G.ToString()),float.Parse(formTicks.Color.B.ToString()));
			}

			if (propertyName ==TickSettings.ThicknessProperty.PropertyName)
			{
				nativeTicks.Thickness = float.Parse(((formTicks.Thickness/ 1000) / 2).ToString());
			}



		}
	}
}

