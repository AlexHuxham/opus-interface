#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Syncfusion.SfGauge.XForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using System.Collections.ObjectModel;

namespace GaugeGettingStarted
{
    public class App
    {
        public static SfCircularGauge circularGauge;
        
        public static Page GetMainPage()
        {      
            var contentPage = new ContentPage
            {

            };
            circularGauge = new SfCircularGauge();
			circularGauge.BackgroundColor = Color.FromRgb(0,0,0);
			ObservableCollection<Scale> scales = new ObservableCollection<Scale>();
            Scale scale = new Scale();
            scale.StartValue= 0;
            scale.EndValue =100;
            scale.Interval = 10;
            scale.StartAngle =135;
            scale.SweepAngle =270;
            scale.RimThickness = 10;
            scale.LabelPostfix = "%";
            scale.RimColor = Color.FromHex("#FB0101");
            scale.LabelColor = Color.White; 
            scale.MinorTicksPerInterval = 3;
			List<Pointer> pointers = new List<Pointer>();
			NeedlePointer needlePointer = new NeedlePointer();
            needlePointer.Value = 60;
			needlePointer.Color = Color.White;
			needlePointer.KnobRadius = 20;
            needlePointer.KnobColor = Color.White;
			needlePointer.Thickness = 5;
			needlePointer.LengthFactor = 0.8;
			needlePointer.Type = PointerType.Bar;
			RangePointer rangePointer = new RangePointer();
            rangePointer.Value = 60;
			rangePointer.Color =Color.FromRgb (169, 169, 169);
			rangePointer.Thickness = 10;
			pointers.Add(needlePointer);
            pointers.Add(rangePointer);
			TickSettings minor = new TickSettings ();
			minor.Length =4;
			minor.Color = Color.White;
			minor.Thickness = 3;
            scale.MinorTickSettings = minor;
			TickSettings major = new TickSettings ();
			major.Length = 7;
			major.Color = Color.White;
			major.Thickness = 3;
            scale.MajorTickSettings = major ;
            scale.Pointers =  pointers;
            scales.Add(scale);
			circularGauge.Scales = scales;
            Header header = new Header();
            header.Text = "CPU Usage";
			header.ForegroundColor = Color.White;
			header.Position = Device.OnPlatform(new Point (150, 200),new Point (50, 70), new Point (50, 70));
            circularGauge.Headers.Add(header);
            Label label;
            if (Device.OS == TargetPlatform.iOS)
            {
                label = new Label() { Text = "\n Gauge Getting Started", HeightRequest = 50, Font = Font.OfSize("Bold", 25), TextColor = Color.White };
            }
            else
            {
                label = new Label() { Text = "Gauge Getting Started", HeightRequest = 50, Font = Font.OfSize("Bold", 25), TextColor = Color.White };
            }
            var mainStack = new StackLayout
            {
                Spacing = 10,
                Padding = Device.OnPlatform(iOS: 10, Android : 10, WinPhone : 50),
                Children = { label, circularGauge }
            };
            contentPage.Content = mainStack;
			contentPage.BackgroundColor = Color.Black;
            return contentPage;
        }
    }
}