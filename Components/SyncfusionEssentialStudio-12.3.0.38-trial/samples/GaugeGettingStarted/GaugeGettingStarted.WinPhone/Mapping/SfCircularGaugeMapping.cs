#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Native =  Syncfusion.UI.Xaml.Gauges.SfCircularGauge;

namespace Syncfusion.XForms.SfGauge.WinPhone
{
    public class SfCircularGaugeMapping
    {
        ScaleMapping scaleMapping = new ScaleMapping();
        public void OnGaugePropertiesChanged(string propertyName, Syncfusion.XForms.SfGauge.SfCircularGauge.SfCircularGauge formsGauge, Native nativeGauge)
        {
            scaleMapping.gauge = nativeGauge;
            if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.SfCircularGauge.GaugeHeightProperty.PropertyName)
            {
                nativeGauge.Height = formsGauge.GaugeHeight;
            }
            else if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.SfCircularGauge.GaugeWidthProperty.PropertyName)
            {
                nativeGauge.Width = formsGauge.GaugeWidth;
            }
            else if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.SfCircularGauge.ScalesProperty.PropertyName)
            {
                if (formsGauge.Scales != null)
                {
                    foreach (Syncfusion.XForms.SfGauge.SfCircularGauge.Scale FormScale in formsGauge.Scales)
                    {
                        var newScale = scaleMapping.GetScale(FormScale);
                        nativeGauge.MainScale = newScale;
                    }
                }
            }
        }
    }
}