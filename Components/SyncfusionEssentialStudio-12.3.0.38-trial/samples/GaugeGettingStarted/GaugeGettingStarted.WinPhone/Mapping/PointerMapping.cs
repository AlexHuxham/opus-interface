#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Syncfusion.UI.Xaml.Gauges;
using Native =  Syncfusion.UI.Xaml.Gauges;
using Syncfusion.XForms.SfGauge.SfCircularGauge;
using System.Windows.Media;

namespace Syncfusion.XForms.SfGauge.WinPhone
{
    public class PointerMapping
    {
        public Native.SfCircularGauge gauge;
        public CircularPointer GetPointer(Syncfusion.XForms.SfGauge.SfCircularGauge.Pointer formPointer)
        {
            CircularPointer newPointer = new CircularPointer();
            
            if (formPointer is Syncfusion.XForms.SfGauge.SfCircularGauge.Pointer)
            {
                List<string> properties = SfGaugeRenderer.GetPropertiesChanged(typeof(Syncfusion.XForms.SfGauge.SfCircularGauge.Pointer), formPointer);
                foreach (var propertyName in properties)
                {
                    OnPointerPropertiesChanged(propertyName, (Syncfusion.XForms.SfGauge.SfCircularGauge.Pointer)formPointer, (CircularPointer)newPointer);
                }
            }
            return newPointer;
        }

        public void OnPointerPropertiesChanged(string propertyName, Syncfusion.XForms.SfGauge.SfCircularGauge.Pointer formPointer, CircularPointer nativePointer)
        {
            if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.Pointer.ValueProperty.PropertyName)
            {
                nativePointer.Value = formPointer.Value;
                if (this.gauge != null)
                {
                    if (this.gauge.MainScale != null && this.gauge.MainScale.Pointers != null && this.gauge.MainScale.Pointers.Count > 0)
                    {
                        this.gauge.MainScale.Pointers[0].Value = formPointer.Value;
                        this.gauge.MainScale.Pointers[1].Value = formPointer.Value;
                    }
                }
            }
            else if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.Pointer.RangePointerThicknessProperty.PropertyName)
            {
                nativePointer.RangePointerStrokeThickness = formPointer.RangePointerThickness;
            }
            else if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.Pointer.RangePointerColorProperty.PropertyName)
            {
                nativePointer.RangePointerStroke = new SolidColorBrush(SfGaugeRenderer.GetWindowsColor(formPointer.RangePointerColor));
            }
            else if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.Pointer.PointerKnobColorProperty.PropertyName)
            {
                nativePointer.PointerCapStroke = new SolidColorBrush(SfGaugeRenderer.GetWindowsColor(formPointer.PointerKnobColor));
            }
            else if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.Pointer.NeedlePointerColorProperty.PropertyName)
            {
                nativePointer.NeedlePointerStroke = new SolidColorBrush(SfGaugeRenderer.GetWindowsColor(formPointer.NeedlePointerColor));
            }
            else if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.Pointer.NeedlePointerFactorProperty.PropertyName)
            {
                nativePointer.NeedleLengthFactor = formPointer.NeedlePointerFactor;
            }
            else if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.Pointer.NeedlePointerThicknessProperty.PropertyName)
            {
                nativePointer.NeedlePointerStrokeThickness = formPointer.NeedlePointerThickness;
            }
            else if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.Pointer.PointerKnobRadiusProperty.PropertyName)
            {
                nativePointer.PointerCapDiameter = 2 * formPointer.PointerKnobRadius;
            }
            else if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.Pointer.IsNeedlePointerProperty.PropertyName)
            {
                if (formPointer.IsNeedlePointer)
                {
                    nativePointer.PointerType = PointerType.NeedlePointer;
                }
                else
                {
                    nativePointer.PointerType = PointerType.RangePointer;
                }
            }
        }
    }
}