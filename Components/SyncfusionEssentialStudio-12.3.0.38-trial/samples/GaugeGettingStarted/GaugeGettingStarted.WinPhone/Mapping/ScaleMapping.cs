#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Syncfusion.UI.Xaml.Gauges;
using Native = Syncfusion.UI.Xaml.Gauges;
using System.Windows.Media;

namespace Syncfusion.XForms.SfGauge.WinPhone
{
    public class ScaleMapping
    {
        public Native.SfCircularGauge gauge;
        PointerMapping pointerMapping;
        RangeMapping rangeMapping;
        CircularScale newScale;

        public CircularScale GetScale(Syncfusion.XForms.SfGauge.SfCircularGauge.Scale formScale)
        {
            pointerMapping = new PointerMapping();
            rangeMapping = new RangeMapping();
            newScale = new CircularScale();
            if (formScale is Syncfusion.XForms.SfGauge.SfCircularGauge.Scale)
            {
                List<string> properties = SfGaugeRenderer.GetPropertiesChanged(typeof(Syncfusion.XForms.SfGauge.SfCircularGauge.Scale), formScale);
                foreach (var propertyName in properties)
                {
                    OnScalePropertiesChanged(propertyName, (Syncfusion.XForms.SfGauge.SfCircularGauge.Scale)formScale, (CircularScale)newScale);
                }
            }
            return newScale;
        }

        public void OnScalePropertiesChanged(string propertyName, Syncfusion.XForms.SfGauge.SfCircularGauge.Scale formScale, CircularScale nativeScale)
        {
            if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.Scale.StartAngleProperty.PropertyName)
            {
                nativeScale.StartAngle = formScale.StartAngle;
            }
            else if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.Scale.SweepAngleProperty.PropertyName)
            {
                nativeScale.SweepAngle = formScale.SweepAngle;
            }
            else if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.Scale.StartValueProperty.PropertyName)
            {
                nativeScale.StartValue = formScale.StartValue;
            }
            else if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.Scale.EndValueProperty.PropertyName)
            {
                nativeScale.EndValue = formScale.EndValue;
            }
            else if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.Scale.MajorTicksHeightProperty.PropertyName)
            {
                nativeScale.TickLength = 1.3 * formScale.MajorTicksHeight;
            }
            else if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.Scale.MajorTicksThicknessProperty.PropertyName)
            {
                nativeScale.TickStrokeThickness = formScale.MajorTicksThickness;
            }
            else if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.Scale.MinorTicksHeightProperty.PropertyName)
            {
                nativeScale.SmallTickLength = formScale.MinorTicksHeight;
            }
            else if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.Scale.MinorTicksPerIntervalProperty.PropertyName)
            {
                nativeScale.MinorTicksPerInterval = (int)formScale.MinorTicksPerInterval;
            }
            else if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.Scale.MinorTicksThicknessProperty.PropertyName)
            {
                nativeScale.SmallTickStrokeThickness = formScale.MinorTicksThickness;
            }
            else if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.Scale.RimThicknessProperty.PropertyName)
            {
                nativeScale.RimStrokeThickness = formScale.RimThickness;
            }
            else if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.Scale.LabelSizeProperty.PropertyName)
            {
                nativeScale.FontSize = formScale.LabelSize;
            }
            else if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.Scale.LabelColorProperty.PropertyName)
            {
                nativeScale.LabelStroke = new SolidColorBrush(SfGaugeRenderer.GetWindowsColor(formScale.LabelColor));
            }
            else if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.Scale.RimColorProperty.PropertyName)
            {
                nativeScale.RimStroke = new SolidColorBrush(SfGaugeRenderer.GetWindowsColor(formScale.RimColor));
            }
            else if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.Scale.MinorTickColorProperty.PropertyName)
            {
                nativeScale.SmallTickStroke = new SolidColorBrush(SfGaugeRenderer.GetWindowsColor(formScale.MinorTickColor));
            }
            else if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.Scale.MajorTickColorProperty.PropertyName)
            {
                nativeScale.TickStroke = new SolidColorBrush(SfGaugeRenderer.GetWindowsColor(formScale.MajorTickColor));
            }
            else if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.Scale.PointersProperty.PropertyName)
            {
                if (formScale.Pointers != null)
                {
                    foreach (Syncfusion.XForms.SfGauge.SfCircularGauge.Pointer FormPointer in formScale.Pointers)
                    {
                        var newPointer = pointerMapping.GetPointer(FormPointer);
                        FormPointer.PropertyChanged += FormPointer_PropertyChanged;
                        pointerMapping.gauge = this.gauge;
                        if (!FormPointer.IsNeedlePointer)
                        {
                            //nativeScale.TickPosition = TickPosition.Cross;
                        }
                        nativeScale.Pointers.Add(newPointer);
                    }
                }
            }
            else if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.Scale.RangesProperty.PropertyName)
            {
                if (formScale.Ranges != null)
                {
                    foreach (Syncfusion.XForms.SfGauge.SfCircularGauge.Range formRange in formScale.Ranges)
                    {
                        var newRange = rangeMapping.GetRange(formRange);
                        nativeScale.Ranges.Add(newRange);
                    }
                }
            }
        }

        void FormPointer_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            CircularPointer newPointer = new CircularPointer();
            if (sender is Syncfusion.XForms.SfGauge.SfCircularGauge.Pointer)
            {
                List<string> properties = SfGaugeRenderer.GetPropertiesChanged(typeof(Syncfusion.XForms.SfGauge.SfCircularGauge.Pointer), sender);
                foreach (var propertyName in properties)
                {
                    this.pointerMapping.OnPointerPropertiesChanged(propertyName, (Syncfusion.XForms.SfGauge.SfCircularGauge.Pointer)sender, (CircularPointer)newPointer);
                }
            }
        }
    }
}