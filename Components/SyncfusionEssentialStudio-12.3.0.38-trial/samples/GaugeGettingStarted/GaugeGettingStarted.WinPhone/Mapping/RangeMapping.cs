#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Syncfusion.UI.Xaml.Gauges;
using System.Windows.Media;

namespace Syncfusion.XForms.SfGauge.WinPhone
{
    public class RangeMapping
    {
        public  CircularRange GetRange(Syncfusion.XForms.SfGauge.SfCircularGauge.Range formRange)
        {
            CircularRange newRange = new CircularRange();
            if (formRange is Syncfusion.XForms.SfGauge.SfCircularGauge.Range)
            {
                List<string> properties = SfGaugeRenderer.GetPropertiesChanged(typeof(Syncfusion.XForms.SfGauge.SfCircularGauge.Range), formRange);
                foreach (var propertyName in properties)
                {
                    OnRangePropertiesChanged(propertyName, (Syncfusion.XForms.SfGauge.SfCircularGauge.Range)formRange, (CircularRange)newRange);
                }
            }
            return newRange;
        }

        public void OnRangePropertiesChanged(string propertyName, Syncfusion.XForms.SfGauge.SfCircularGauge.Range formRange, CircularRange nativeRange)
        {
            if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.Range.StartValueProperty.PropertyName)
            {
                nativeRange.StartValue = formRange.StartValue;
            }
            else if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.Range.EndValueProperty.PropertyName)
            {
                nativeRange.EndValue = formRange.EndValue;
            }
            else if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.Range.RangeColorProperty.PropertyName)
            {
                nativeRange.Stroke = new SolidColorBrush(SfGaugeRenderer.GetWindowsColor(formRange.RangeColor));
            }
            else if (propertyName == Syncfusion.XForms.SfGauge.SfCircularGauge.Range.RangeThicknessProperty.PropertyName)
            {
                nativeRange.StrokeThickness = formRange.RangeThickness;
            }
        }
    }
}