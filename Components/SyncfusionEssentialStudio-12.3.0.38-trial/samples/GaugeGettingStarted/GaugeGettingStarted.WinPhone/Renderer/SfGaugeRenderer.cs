#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

using Native = Syncfusion.UI.Xaml.Gauges.SfCircularGauge;

using Xamarin.Forms;
using Xamarin.Forms.Platform.WinPhone;
using Syncfusion.XForms.SfGauge.SfCircularGauge;


[assembly: ExportRenderer(typeof(Syncfusion.XForms.SfGauge.SfCircularGauge.SfCircularGauge), typeof(Syncfusion.XForms.SfGauge.WinPhone.SfGaugeRenderer))]

namespace Syncfusion.XForms.SfGauge.WinPhone
{
    public class SfGaugeRenderer : ViewRenderer<Syncfusion.XForms.SfGauge.SfCircularGauge.SfCircularGauge, Syncfusion.UI.Xaml.Gauges.SfCircularGauge>
    {
        Syncfusion.XForms.SfGauge.SfCircularGauge.SfCircularGauge gauge;
        Native nativeGauge;
        SfCircularGaugeMapping sfCircularGaugeMapping = new SfCircularGaugeMapping();
        public SfGaugeRenderer()
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<Syncfusion.XForms.SfGauge.SfCircularGauge.SfCircularGauge> e)
        {
            base.OnElementChanged(e);
            gauge = e.NewElement;
            nativeGauge = new Native();
            List<string> propertiesNames = GetPropertiesChanged(typeof(Syncfusion.XForms.SfGauge.SfCircularGauge.SfCircularGauge), gauge);

            foreach (var name in propertiesNames)
            {
                sfCircularGaugeMapping.OnGaugePropertiesChanged(name, gauge, nativeGauge);
            }
            SetNativeControl(nativeGauge);
            propertiesNames.Clear();
        }

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (sender is Syncfusion.XForms.SfGauge.SfCircularGauge.SfCircularGauge)
            {
                sfCircularGaugeMapping.OnGaugePropertiesChanged(e.PropertyName, gauge, nativeGauge);
            }
        }

        public static List<string> GetPropertiesChanged(Type type, Object obj)
        {
            var properties = type.GetProperties();
            List<string> propertiesList = new List<string>();
            foreach (var item in properties)
            {
                propertiesList.Add(item.Name);
            }
            return propertiesList;
            //foreach (PropertyInfo property in type.GetProperties(BindingFlags.Instance | BindingFlags.NonPublic))
            //{
            //    if (property.Name == "PropertiesChanged")
            //    {
            //        return property.GetValue(obj) as List<string>;
            //    }
            //}
            //return null;
        }

        public static System.Windows.Media.Color GetWindowsColor(Xamarin.Forms.Color xColor)
        {
            System.Windows.Media.Color wColor = System.Windows.Media.Color.FromArgb((byte)(xColor.A * 255), (byte)(xColor.R * 255), (byte)(xColor.G * 255), (byte)(xColor.B * 255));
            return wColor;
        }

        public static Color GetColor(float hue, float saturation, float luminosity)
        {
            int r = 0, g = 0, b = 0;
            if (saturation == 0)
            {
                r = g = b = (int)(luminosity * 255.0f + 0.5f);
            }
            else
            {
                float h = (hue - (float)Math.Floor(hue)) * 6.0f;
                float f = h - (float)Math.Floor(h);
                float p = luminosity * (1.0f - saturation);
                float q = luminosity * (1.0f - saturation * f);
                float t = luminosity * (1.0f - (saturation * (1.0f - f)));
                switch ((int)h)
                {
                    case 0:
                        r = (int)(luminosity * 255.0f + 0.5f);
                        g = (int)(t * 255.0f + 0.5f);
                        b = (int)(p * 255.0f + 0.5f);
                        break;
                    case 1:
                        r = (int)(q * 255.0f + 0.5f);
                        g = (int)(luminosity * 255.0f + 0.5f);
                        b = (int)(p * 255.0f + 0.5f);
                        break;
                    case 2:
                        r = (int)(p * 255.0f + 0.5f);
                        g = (int)(luminosity * 255.0f + 0.5f);
                        b = (int)(t * 255.0f + 0.5f);
                        break;
                    case 3:
                        r = (int)(p * 255.0f + 0.5f);
                        g = (int)(q * 255.0f + 0.5f);
                        b = (int)(luminosity * 255.0f + 0.5f);
                        break;
                    case 4:
                        r = (int)(t * 255.0f + 0.5f);
                        g = (int)(p * 255.0f + 0.5f);
                        b = (int)(luminosity * 255.0f + 0.5f);
                        break;
                    case 5:
                        r = (int)(luminosity * 255.0f + 0.5f);
                        g = (int)(p * 255.0f + 0.5f);
                        b = (int)(q * 255.0f + 0.5f);
                        break;
                }
            }
            return new Color(r, g, b);
        }
    }
}