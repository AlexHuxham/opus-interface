#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Native = Com.Syncfusion.Widget.Gauge.SfCircularGauge;

using Xamarin.Forms.Platform.Android;
using System.Reflection;
using Syncfusion.XForms.SfGauge.Droid;

[assembly: ExportRenderer(typeof(Syncfusion.XForms.SfGauge.SfCircularGauge.SfCircularGauge), typeof(Syncfusion.XForms.SfGauge.Droid.SfGaugeRenderer))]

namespace Syncfusion.XForms.SfGauge.Droid
{
    public class SfGaugeRenderer : ViewRenderer<Syncfusion.XForms.SfGauge.SfCircularGauge.SfCircularGauge, Com.Syncfusion.Widget.Gauge.SfCircularGauge>
    {
        SfCircularGauge.SfCircularGauge gauge;
        public Native nativeGauge;
        public SfGaugeRenderer()
        {

        }
        SfCircularGaugeMapping sfCircularGaugeMapping = new SfCircularGaugeMapping();
        protected override void OnElementChanged(ElementChangedEventArgs<Syncfusion.XForms.SfGauge.SfCircularGauge.SfCircularGauge> e)
        {
            base.OnElementChanged(e);
            gauge = e.NewElement;
            nativeGauge = new Native(Forms.Context);
            List<string> propertiesNames = GetPropertiesChanged(typeof(SfCircularGauge.SfCircularGauge), gauge);
            foreach (var name in propertiesNames)
            {
                sfCircularGaugeMapping.OnGaugePropertiesChanged(name, gauge, nativeGauge);
            }
          
            SetNativeControl(nativeGauge);
            propertiesNames.Clear();
        }

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            SfCircularGaugeMapping sfCircularGaugeMapping;
            if (sender is SfCircularGauge.SfCircularGauge)
            {
                sfCircularGaugeMapping = new SfCircularGaugeMapping();
                sfCircularGaugeMapping.OnGaugePropertiesChanged(e.PropertyName, gauge, nativeGauge);
            }
        }

        public static List<string> GetPropertiesChanged(Type type, Object obj)
        {
            var properties = type.GetProperties();
            List<string> propertiesList = new List<string>();
            foreach (var item in properties)
            {
                propertiesList.Add(item.Name);
            }
            return propertiesList;
        }


        public static Android.Graphics.Color GetRGB(float hue, float saturation, float luminosity)
        {
            int r = 0, g = 0, b = 0;
            if (saturation == 0)
            {
                r = g = b = (int)(luminosity * 255.0f + 0.5f);
            }
            else
            {
                float h = (hue - (float)Math.Floor(hue)) * 6.0f;
                float f = h - (float)Math.Floor(h);
                float p = luminosity * (1.0f - saturation);
                float q = luminosity * (1.0f - saturation * f);
                float t = luminosity * (1.0f - (saturation * (1.0f - f)));
                switch ((int)h)
                {
                    case 0:
                        r = (int)(luminosity * 255.0f + 0.5f);
                        g = (int)(t * 255.0f + 0.5f);
                        b = (int)(p * 255.0f + 0.5f);
                        break;
                    case 1:
                        r = (int)(q * 255.0f + 0.5f);
                        g = (int)(luminosity * 255.0f + 0.5f);
                        b = (int)(p * 255.0f + 0.5f);
                        break;
                    case 2:
                        r = (int)(p * 255.0f + 0.5f);
                        g = (int)(luminosity * 255.0f + 0.5f);
                        b = (int)(t * 255.0f + 0.5f);
                        break;
                    case 3:
                        r = (int)(p * 255.0f + 0.5f);
                        g = (int)(q * 255.0f + 0.5f);
                        b = (int)(luminosity * 255.0f + 0.5f);
                        break;
                    case 4:
                        r = (int)(t * 255.0f + 0.5f);
                        g = (int)(p * 255.0f + 0.5f);
                        b = (int)(luminosity * 255.0f + 0.5f);
                        break;
                    case 5:
                        r = (int)(luminosity * 255.0f + 0.5f);
                        g = (int)(p * 255.0f + 0.5f);
                        b = (int)(q * 255.0f + 0.5f);
                        break;
                }
            }
            return new Android.Graphics.Color(r,g,b); 
        }
    }
}