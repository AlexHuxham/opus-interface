#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Com.Syncfusion.Widget.Gauge;

namespace Syncfusion.XForms.SfGauge.Droid
{
    public  class RangeMapping
    {
        Range newRange = new Range();
        public  Range GetRange(SfCircularGauge.Range formRange)
        {
            if (formRange is SfCircularGauge.Range)
            {
                List<string> properties = SfGaugeRenderer.GetPropertiesChanged(typeof(SfCircularGauge.Range), formRange);
                foreach (var propertyName in properties)
                {
                    OnRangePropertiesChanged(propertyName, (SfCircularGauge.Range)formRange, (Range)newRange);
                }
            }
            return newRange;
        }

        public  void OnRangePropertiesChanged(string propertyName, SfCircularGauge.Range formRange, Range nativeRange)
        {
            if (propertyName == SfCircularGauge.Range.StartValueProperty.PropertyName)
            {
                nativeRange.RangeStartValue = formRange.StartValue;
            }
            else if (propertyName == SfCircularGauge.Range.EndValueProperty.PropertyName)
            {
                nativeRange.RangeEndValue = formRange.EndValue;
            }
            else if (propertyName == SfCircularGauge.Range.RangeColorProperty.PropertyName)
            {
                nativeRange.RangeColor = SfGaugeRenderer.GetRGB((float)formRange.RangeColor.Hue, (float)formRange.RangeColor.Saturation, (float)formRange.RangeColor.Luminosity);
            }
            else if (propertyName == SfCircularGauge.Range.RangeThicknessProperty.PropertyName)
            {
                nativeRange.RangeThickness = formRange.RangeThickness;
            }
        }
    }
}