#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using native = Com.Syncfusion.Widget.Gauge;
using Com.Syncfusion.Widget.Gauge;

namespace Syncfusion.XForms.SfGauge.Droid
{
   public class ScaleMapping
    {
        internal native.SfCircularGauge gauge { get; set; }
        PointerMapping pointerMapping = new PointerMapping();
        Scale newScale = new Scale();
        Pointer newPointer = new Pointer();
        
       public  Scale GetScale(SfCircularGauge.Scale formScale)
        {
            if (formScale is SfCircularGauge.Scale)
            {
                List<string> properties = SfGaugeRenderer.GetPropertiesChanged(typeof(SfCircularGauge.Scale), formScale);
                foreach (var propertyName in properties)
                {
                    OnScalePropertiesChanged(propertyName, (SfCircularGauge.Scale)formScale, (Scale)newScale);
                }
            }
            return newScale;
        }

        public  void OnScalePropertiesChanged(string propertyName, SfCircularGauge.Scale formScale, Scale nativeScale)
        {
            if (propertyName == SfCircularGauge.Scale.StartAngleProperty.PropertyName)
            {
                nativeScale.StartAngle = formScale.StartAngle;
            }
            else if (propertyName == SfCircularGauge.Scale.IntervalProperty.PropertyName)
            {
                nativeScale.Interval = formScale.Interval;
            }
            else if (propertyName == SfCircularGauge.Scale.SweepAngleProperty.PropertyName)
            {
                nativeScale.SweepAngle = formScale.SweepAngle;
            }
            else if (propertyName == SfCircularGauge.Scale.StartValueProperty.PropertyName)
            {
                nativeScale.StartValue = formScale.StartValue;
            }
            else if (propertyName == SfCircularGauge.Scale.EndValueProperty.PropertyName)
            {
                nativeScale.EndValue = formScale.EndValue;
            }
            else if (propertyName == SfCircularGauge.Scale.MajorTicksHeightProperty.PropertyName)
            {
                nativeScale.MajorTickHeight = formScale.MajorTicksHeight;
            }
            else if (propertyName == SfCircularGauge.Scale.MajorTicksThicknessProperty.PropertyName)
            {
                nativeScale.MajorTickThickness = formScale.MajorTicksThickness;
            }
            else if (propertyName == SfCircularGauge.Scale.MinorTicksHeightProperty.PropertyName)
            {
                nativeScale.MinorTickHeight = formScale.MinorTicksHeight;
            }
            else if (propertyName == SfCircularGauge.Scale.MinorTicksPerIntervalProperty.PropertyName)
            {
                nativeScale.MinorTicksPerInterval = formScale.MinorTicksPerInterval;
            }
            else if (propertyName == SfCircularGauge.Scale.MinorTicksThicknessProperty.PropertyName)
            {
                nativeScale.MinorTickThickness = formScale.MinorTicksThickness;
            }
            else if (propertyName == SfCircularGauge.Scale.RimThicknessProperty.PropertyName)
            {
                nativeScale.RimThickness = formScale.RimThickness;
            }
            else if (propertyName == SfCircularGauge.Scale.LabelSizeProperty.PropertyName)
            {
                nativeScale.LabelSize = formScale.LabelSize;
            }
            else if (propertyName == SfCircularGauge.Scale.LabelColorProperty.PropertyName)
            {
                nativeScale.LabelColor = SfGaugeRenderer.GetRGB((float)formScale.LabelColor.Hue, (float)formScale.LabelColor.Saturation, (float)formScale.LabelColor.Luminosity);
            }
            else if (propertyName == SfCircularGauge.Scale.RimColorProperty.PropertyName)
            {
                nativeScale.RimColor = SfGaugeRenderer.GetRGB((float)formScale.RimColor.Hue, (float)formScale.RimColor.Saturation, (float)formScale.RimColor.Luminosity);
            }
            else if (propertyName == SfCircularGauge.Scale.MinorTickColorProperty.PropertyName)
            {
                nativeScale.MinorTickColor = SfGaugeRenderer.GetRGB((float)formScale.MinorTickColor.Hue, (float)formScale.MinorTickColor.Saturation, (float)formScale.MinorTickColor.Luminosity);
            }
            else if (propertyName == SfCircularGauge.Scale.MajorTickColorProperty.PropertyName)
            {
                nativeScale.MajorTickColor = SfGaugeRenderer.GetRGB((float)formScale.MajorTickColor.Hue, (float)formScale.MajorTickColor.Saturation, (float)formScale.MajorTickColor.Luminosity);
            }
            else if (propertyName == SfCircularGauge.Scale.PointersProperty.PropertyName)
            {
                if (formScale.Pointers != null)
                {
                    foreach (SfCircularGauge.Pointer FormPointer in formScale.Pointers)
                    {
                        pointerMapping.gauge = this.gauge;
                        var newPointer = pointerMapping.GetPointer(FormPointer);
                        FormPointer.PropertyChanged += FormPointer_PropertyChanged;
                        nativeScale.Pointers.Add(newPointer);
                    }
                }
            }
            else if (propertyName == SfCircularGauge.Scale.RangesProperty.PropertyName)
            {
                if (formScale.Ranges != null)
                {
                    RangeMapping rangeMapping;
                    foreach (SfCircularGauge.Range formRange in formScale.Ranges)
                    {
                        rangeMapping = new RangeMapping();
                        var newRange = rangeMapping.GetRange(formRange);
                        nativeScale.Ranges.Add(newRange);
                    }
                }
            }
        }

       void FormPointer_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (sender is SfCircularGauge.Pointer)
            {
                List<string> properties = SfGaugeRenderer.GetPropertiesChanged(typeof(SfCircularGauge.Pointer), sender);
                foreach (var propertyName in properties)
                {
                    pointerMapping.OnPointerPropertiesChanged(propertyName, (SfCircularGauge.Pointer)sender, (Pointer)newPointer);
                }
            }
        }
    }
}