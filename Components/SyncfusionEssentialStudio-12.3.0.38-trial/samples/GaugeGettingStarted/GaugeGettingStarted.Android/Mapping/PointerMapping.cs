#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Native = Com.Syncfusion.Widget.Gauge;
using Com.Syncfusion.Widget.Gauge;
using Com.Syncfusion.Widget.Gauge.Enums;

namespace Syncfusion.XForms.SfGauge.Droid
{
    public class PointerMapping
    {
        public Native.SfCircularGauge gauge;
        public Pointer GetPointer(SfCircularGauge.Pointer formPointer)
        {
            Pointer newPointer = new Pointer();
            if (formPointer is SfCircularGauge.Pointer)
            {
                List<string> properties = SfGaugeRenderer.GetPropertiesChanged(typeof(SfCircularGauge.Pointer), formPointer);
                foreach (var propertyName in properties)
                {
                    OnPointerPropertiesChanged(propertyName, (SfCircularGauge.Pointer)formPointer, (Pointer)newPointer);
                }
            }
            return newPointer;
        }

        public void OnPointerPropertiesChanged(string propertyName, SfCircularGauge.Pointer formPointer, Pointer nativePointer)
        {
            if (propertyName == SfCircularGauge.Pointer.ValueProperty.PropertyName)
            {
                nativePointer.Value = formPointer.Value;
                if (this.gauge != null)
                {
                    if (this.gauge.Scales != null && this.gauge.Scales.Count > 0 && this.gauge.Scales[0].Pointers != null && this.gauge.Scales[0].Pointers.Count > 0)
                    {
                        this.gauge.Scales[0].Pointers[0].Value = formPointer.Value;
                        this.gauge.Scales[0].Pointers[1].Value = formPointer.Value;
                    }
                    this.gauge.RefreshGauge();
                }
            }
            if (propertyName == SfCircularGauge.Pointer.RangePointerThicknessProperty.PropertyName)
            {
                nativePointer.RangePointerThickness = formPointer.RangePointerThickness;
            }
            else if (propertyName == SfCircularGauge.Pointer.RangePointerColorProperty.PropertyName)
            {
                nativePointer.RangePointerColor = SfGaugeRenderer.GetRGB((float)formPointer.RangePointerColor.Hue, (float)formPointer.RangePointerColor.Saturation, (float)formPointer.RangePointerColor.Luminosity);
            }
            else if (propertyName == SfCircularGauge.Pointer.PointerKnobColorProperty.PropertyName)
            {
                nativePointer.PointerKnobColor = SfGaugeRenderer.GetRGB((float)formPointer.PointerKnobColor.Hue, (float)formPointer.PointerKnobColor.Saturation, (float)formPointer.PointerKnobColor.Luminosity);
            }
            else if (propertyName == SfCircularGauge.Pointer.NeedlePointerColorProperty.PropertyName)
            {
                nativePointer.PointerNeedleColor = SfGaugeRenderer.GetRGB((float)formPointer.NeedlePointerColor.Hue, (float)formPointer.NeedlePointerColor.Saturation, (float)formPointer.NeedlePointerColor.Luminosity);
            }
            else if (propertyName == SfCircularGauge.Pointer.NeedlePointerFactorProperty.PropertyName)
            {
                nativePointer.NeedlePointerFactor = formPointer.NeedlePointerFactor;
            }
            else if (propertyName == SfCircularGauge.Pointer.NeedlePointerThicknessProperty.PropertyName)
            {
                nativePointer.NeedleType = NeedleType.Triangle;
                nativePointer.PointerThickness = formPointer.NeedlePointerThickness;
            }
            else if (propertyName == SfCircularGauge.Pointer.PointerKnobRadiusProperty.PropertyName)
            {
                nativePointer.PointerKnobSize = formPointer.PointerKnobRadius;
            }
            else if (propertyName == SfCircularGauge.Pointer.IsNeedlePointerProperty.PropertyName)
            {
                if (formPointer.IsNeedlePointer)
                {
                    nativePointer.PointerType = PointerType.Needle;
                }
                else
                {
                    nativePointer.PointerType = PointerType.Range;
                }
            }
        }
    }
}