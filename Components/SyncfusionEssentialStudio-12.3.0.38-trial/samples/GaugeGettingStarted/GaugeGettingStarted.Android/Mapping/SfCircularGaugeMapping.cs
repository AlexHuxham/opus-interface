#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Native =  Com.Syncfusion.Widget.Gauge;
using Com.Syncfusion.Widget.Gauge;

namespace Syncfusion.XForms.SfGauge.Droid
{
    public  class SfCircularGaugeMapping
    {
        Native.SfCircularGauge gauge;
        ScaleMapping scaleMapping;
        Scale newScale = new Scale();
        
        public  void OnGaugePropertiesChanged(string propertyName, SfCircularGauge.SfCircularGauge formsGauge, Native.SfCircularGauge nativeGauge )
        {
            gauge = nativeGauge;
            scaleMapping = new ScaleMapping();
            scaleMapping.gauge = gauge;   
            formsGauge.PropertyChanged += formsGauge_PropertyChanged;
            if (propertyName == SfCircularGauge.SfCircularGauge.GaugeHeightProperty.PropertyName)
            {
                nativeGauge.GaugeHeight = formsGauge.GaugeHeight;
            }
            else if (propertyName == SfCircularGauge.SfCircularGauge.GaugeWidthProperty.PropertyName)
            {
                nativeGauge.GaugeWidth = formsGauge.GaugeWidth;
            }
            else if (propertyName == SfCircularGauge.SfCircularGauge.BackColorProperty.PropertyName)
            {
                nativeGauge.BackColor = SfGaugeRenderer.GetRGB((float)formsGauge.BackColor.Hue, (float)formsGauge.BackColor.Saturation, (float)formsGauge.BackColor.Luminosity);
            }
            else if (propertyName == SfCircularGauge.SfCircularGauge.GaugeHeadersProperty.PropertyName)
            {
                if (formsGauge.GaugeHeaders != null)
                {
                    GaugeHeaderMapping gaugeHeaderMapping;
                    foreach (SfCircularGauge.GaugeHeader FormHeader in formsGauge.GaugeHeaders)
                    {
                        gaugeHeaderMapping = new GaugeHeaderMapping();
                        var newHeader = gaugeHeaderMapping.GetHeader(FormHeader);
                        nativeGauge.GaugeHeaders.Add(newHeader);
                    }
                }
            }
            else if (propertyName == SfCircularGauge.SfCircularGauge.ScalesProperty.PropertyName)
            {
                if (formsGauge.Scales != null)
                {
                    foreach (SfCircularGauge.Scale FormScale in formsGauge.Scales)
                    {
                        scaleMapping.gauge = nativeGauge;
                        var newScale = scaleMapping.GetScale(FormScale);
                        FormScale.PropertyChanged += FormScale_PropertyChanged;
                        FormScale.BindingContext = formsGauge.BindingContext;
                        nativeGauge.Scales.Add(newScale);
                    }
                }
            }
        }

         void formsGauge_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (sender is SfCircularGauge.SfCircularGauge)
            {
            }
        }
         
         void FormScale_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (sender is SfCircularGauge.Scale)
            {
                List<string> properties = SfGaugeRenderer.GetPropertiesChanged(typeof(SfCircularGauge.Scale), sender);
                foreach (var propertyName in properties)
                {
                    // scaleMapping.OnScalePropertiesChanged(propertyName, (SfCircularGauge.Scale)sender, (Scale)newScale);
                }
                if (gauge != null)
                {
                    gauge.RefreshGauge();
                }
            }
        }
    }
}