#region Copyright Syncfusion Inc. 2001 - 2014
// Copyright Syncfusion Inc. 2001 - 2014. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Com.Syncfusion.Widget.Gauge;

namespace Syncfusion.XForms.SfGauge.Droid
{
    public  class GaugeHeaderMapping
    {
        public  GaugeHeader GetHeader(SfCircularGauge.GaugeHeader gaugeHeader)
        {
            GaugeHeader  newHeader = new GaugeHeader();
            if (gaugeHeader is SfCircularGauge.GaugeHeader)
            {
                List<string> properties = SfGaugeRenderer.GetPropertiesChanged(typeof(SfCircularGauge.GaugeHeader), gaugeHeader);
                foreach (var propertyName in properties)
                {
                    OnGaugeHeaderPropertiesChanged(propertyName, (SfCircularGauge.GaugeHeader)gaugeHeader, (GaugeHeader)newHeader);
                }
            }
            return newHeader;
        }

        public  void OnGaugeHeaderPropertiesChanged(string propertyName, SfCircularGauge.GaugeHeader formHeader, GaugeHeader nativeHeader)
        {
            if (propertyName == SfCircularGauge.GaugeHeader.HeaderColorProperty.PropertyName)
            {
                nativeHeader.HeaderColor = SfGaugeRenderer.GetRGB((float)formHeader.HeaderColor.Hue, (float)formHeader.HeaderColor.Saturation, (float)formHeader.HeaderColor.Luminosity);
            }
            else if (propertyName == SfCircularGauge.GaugeHeader.HeaderSizeProperty.PropertyName)
            {
                nativeHeader.HeaderTextSize = formHeader.HeaderSize;
            }
            else if (propertyName == SfCircularGauge.GaugeHeader.PositionXProperty.PropertyName)
            {
                nativeHeader.PositionX = formHeader.PositionX;
            }
            else if (propertyName == SfCircularGauge.GaugeHeader.PositionYProperty.PropertyName)
            {
                nativeHeader.PositionY = formHeader.PositionY;
            }
            else if (propertyName == SfCircularGauge.GaugeHeader.HeaderTextProperty.PropertyName)
            {
                nativeHeader.GaugeHeaderText = formHeader.HeaderText;
            }
        }
    }
}