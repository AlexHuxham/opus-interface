﻿This section contains getting started tutorials that provide a quick overview for working with our components. The goal is to get you up and running as soon as possible.

**Data Visualization**

  [__Chart__](#Chart)

  [__Gauge__](#Gauge)

  [__TreeMap__](#TreeMap)

**File Format**

  [__XlsIO__](#XlsIO)

  [__DocIO__](#DocIO)

  [__PDF__](#PDF)

#<a id="Chart"></a>Creating your first Chart in Xamarin.Forms

This section provides a quick overview for working with Essential Chart for Xamarin.Forms. We will walk through the entire process of creating a real world chart.

The goal of this tutorial is to visualize the weather data for Washington, DC during the period 1961-1990. The raw sample data is given below.

| Month | High | Low | Precipitation |
| --- | --- | --- | --- |
| January | 42 | 27 | 3.03 |
| February | 44 | 28 | 2.48 |
| March | 53 | 35 | 3.23 |
| April | 64 | 44 | 3.15 |
| May | 75 | 54 | 4.13 |
| June | 83 | 63 | 3.23 |
| July | 87 | 68 | 4.13 |
| August | 84 | 66 | 4.88 |
| September | 78 | 59 | 3.82 |
| October | 67 | 48 | 3.07 |
| November | 55 | 38 | 2.83 |
| December | 45 | 29 | 2.8 |

Table 1: Sample weather data.

This is how the final output will look like on iOS, Android and Windows Phone devices. You can also download the entire source code of this demo from [here](http://files2.syncfusion.com/Installs/v12.2.0.40/Samples/Xamarin/Chart_GettingStarted.zip).

![chart](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/chart/gettingstarted/weatheranalysis.png)

**Referencing Essential Studio components in your solution**

If you had acquired Essential Studio components through the Xamarin component store interface from within your IDE, then after adding the components to your Xamarin.iOS, Xamarin.Android and Windows Phone projects through the Component manager, you will still need to manually reference the PCL (Portable Class Library) assemblies in the Xamarin.Forms PCL project in your solution. You can do this by manually adding the relevant PCL assembly references to your PCL project contained in the following path inside of your solution folder  

```
Components/syncfusionessentialstudio-version/lib/pcl/
```

Alternatively if you had downloaded Essential Studio from Syncfusion.com or through the Xamarin store web interface then all assembly references need to be added manually.  

After installing Essential Studio for Xamarin, all the required assemblies can be found in the installation folders, typically
	
**{Syncfusion Installed location}\Essential Studio\12.2.0.40\lib**

_Eg: C:\Program Files (x86)\Syncfusion\Essential Studio\12.2.0.40\lib_

or after downloading through the Xamarin store web interface, all the required assemblies can be found in the below folder

**{download location}\syncfusionessentialstudio-version\lib**

You can then add the assembly references to the respective projects as shown below

**PCL project**

```
pcl\Syncfusion.SfChart.XForm.dll 
```

**Android project**

```
android\Syncfusion.SfChart.Andriod.dll
android\Syncfusion.SfChart.xForms.Andriod.dll
```

**iOS project**

```
ios\Syncfusion.SfChart.iOS.dll 
ios\Syncfusion.SfChart.xForms.iOS.dll
ios\Syncfusion.SfChart.XForm.dll
```

**Windows Phone project**

```
wp8\Syncfusion.SfChart.WP8.dll
wp8\Syncfusion.SfChart.xForms.WinPhone.dll
```

Note: Essential Chart for Xamarin is compatible with Xamarin.Forms v1.2.3.6257.

Currently an additional step is required for configuring Windows Phone and iOS projects. We need to create an instance of the chart custom renderer as shown below.

Create an instance of SfChartRenderer in the MainPage constructor of the Windows Phone project as shown below

```csharp
public MainPage()
{
	new SfChartRenderer();
	...  
}
```

Create an instance of SfChartRenderer in FinishedLaunching overridden method of AppDelegate class in the iOS Project as shown below

```csharp
public override bool FinishedLaunching(UIApplication app, NSDictionary options)
{
	new SfChartRenderer();
	...  
}
```

**Adding and configuring the chart**

The chart control can be configured entirely in C# code or using XAML markup.

1. Create an instance of SfChart
2. Add the primary and secondary axis for the chart as shown below.

C#

```csharp
SfChart chart = new SfChart ();

//Initializing Primary Axis   
CategoryAxisprimaryAxis=newCategoryAxis();
primaryAxis.Title=newChartAxisTitle(){Text="Month"};
chart.PrimaryAxis=primaryAxis;

//Initializing Secondary Axis
NumericalAxissecondaryAxis=newNumericalAxis();
secondaryAxis.Title=newChartAxisTitle(){Text="Temperature"};
chart.SecondaryAxis=secondaryAxis;

this.Content = chart;
```

XAML 

```xml
<chart:SfChart>
	<chart:SfChart.PrimaryAxis>
		<chart:CategoryAxis>
			<chart:CategoryAxis.Title>
				<chart:ChartAxisTitle Text="Month"/>
			</chart:CategoryAxis.Title>
		</chart:CategoryAxis>
	</chart:SfChart.PrimaryAxis>

	<chart:SfChart.SecondaryAxis>
		<chart:NumericalAxis>
			<chart:NumericalAxis>
				<chart:ChartAxisTitle Text="Month"/>
			</chart:NumericalAxis>
		</chart:NumericalAxis>
	</chart:SfChart.SecondaryAxis>
</chart:SfChart>
```

A title for the chart is set using the Title property as shown below,

C#

```csharp
chart.Title=newChartTitle(){Text="WeatherAnalysis"};
```

XAML

```xml
<chart:SfChart>
	<chart:SfChart.Title>
		<chart:ChartTitle Text="Weather Analysis"/>
	</chart:SfChart.Title>
	<!-- ... -->
</chart:SfChart>
```

**Add Chart series**

In this sample we will be visualizing the temperature over the months using a Column Series. Before creating the series, let's create a data model representing the climate details data.

In SfChart, the series itemsource needs to be a collection of _ChartDataPoint_ objects. Add the following class for generating the datapoints.

```csharp
publicclassDataModel
{
	publicObservableCollectionHighTemperature;

	publicDataModel()
	{
		HighTemperature=newObservableCollection();

		HighTemperature.Add(newChartDataPoint("Jan",42));
		HighTemperature.Add(newChartDataPoint("Feb",44));
		HighTemperature.Add(newChartDataPoint("Mar",53));
		HighTemperature.Add(newChartDataPoint("Apr",64));
		HighTemperature.Add(newChartDataPoint("May",75));
		HighTemperature.Add(newChartDataPoint("Jun",83));
		HighTemperature.Add(newChartDataPoint("Jul",87));
		HighTemperature.Add(newChartDataPoint("Aug",84));
		HighTemperature.Add(newChartDataPoint("Sep",78));
		HighTemperature.Add(newChartDataPoint("Oct",67));
		HighTemperature.Add(newChartDataPoint("Nov",55));
		HighTemperature.Add(newChartDataPoint("Dec",45));
	}
}    
```
   

Now add the series to the chart and set its ItemsSource as shown below

C#

```csharp
//Adding the series to the chart

chart.Series.Add(newColumnSeries(){
	ItemsSource=dataModel.HighTemperature
});
```

XAML

```xml
<chart:SfChart>
	<!-- ... -->	
	<chart:SfChart.Series>
		<chart:ColumnSeries ItemsSource = "{Binding HighTemperature}"/>				
	</chart:SfChart.Series>
</chart:SfChart>
```
	
**Adding Legends**

Legends can be enabled in SfChart by initializing the _Legend_ property with _ChartLegend_ instance as shown below

C#

```csharp
    //Adding Legend
    chart.Legend=newChartLegend();
```

XAML

```xml
<chart:SfChart>
	<chart:SfChart.Legend>
		<chart:ChartLegend/>
	</chart:SfChart.Legend>
	<!-- ... -->
</chart:SfChart>
```

Circular legend icons will be displayed for each series by default. Next, we need to provide the labels for the series using the Label property, this information will be displayed along the legend icon.

The next step is to add the HighTemperature column series as shown below

C#

```csharp
//Adding the column series to the chart
chart.Series.Add(newColumnSeries(){ItemsSource=dataModel.HighTemperature,
	Label="Series1"
});
```

XAML

```xml
<chart:SfChart>
	<!-- ... -->
	<chart:SfChart.Series>
		<chart:ColumnSeries Label = "Series 1" ItemsSource =
			"{Binding HighTemperature}"/>
	</chart:SfChart.Series>
</chart:SfChart>
```

**Adding multiple series to the chart**

So far we have visualized just the high temperature data over time. Now let's visualize other data such as low temperature and precipitation.

Let's add two _SplineSeries_ for displaying high and low temperatures and a _ColumnSeries_ for displaying the precipitation as shown below

C#

```csharp
DataModeldataModel=newDataModel();

//Adding Column Series to the chart for displaying precipitation
chart.Series.Add(newColumnSeries(){
	ItemsSource=dataModel.Precipitation,
	Label="Precipitation"
});

//Adding the Spline Series to the chart for displaying high temperature
chart.Series.Add(newSplineSeries(){
	ItemsSource=dataModel.HighTemperature,
	Label="High"
});

//Adding the Spline Series to the chart for displaying low temperature
chart.Series.Add(newSplineSeries(){
	ItemsSource=dataModel.LowTemperature,
	Label="Low"
});
```

XAML

```xml
<chart:SfChart>
	<!-- ... -->	
	<chart:SfChart.Series>
		<chart:ColumnSeries   Label = "Low" ItemsSource =
			"{Binding Precipitation}"/>
		<chart:SplineSeries  Label = "High" ItemsSource =
			"{Binding HighTemperature}"/>
		<chart:SplineSeries  Label = "Low" ItemsSource =
			"{Binding LowTemperature}"/>
	</chart:SfChart.Series>
</chart:SfChart>
```

Currently all the data is plotted against a single scale but the precipitation data needs to be plotted against a different scale.

**Adding multiple Y-axis**

Let's add a secondary axis(y axis) to the chart as shown below

C#
	
```csharp
//Adding Column Series to the chart for displaying precipitation
chart.Series.Add(newColumnSeries(){
	ItemsSource=dataModel.Precipitation,
	Label="Precipitation",
	YAxis=newNumericalAxis(){OpposedPosition=true}
);
```

XAML

```xml
	<chart:SfChart>
	<!-- ... -->
	<chart:ColumnSeries   Label = "Low" ItemsSource =
		"{Binding Precipitation}">
		<chart:ColumnSeries.YAxis>
			<chart:NumericalAxis OpposedPosition ="true"/>
		</chart:ColumnSeries.YAxis>
	</chart:ColumnSeries>
	<!-- ... -->
</chart:SfChart>
```

The _OpposedPostion_ has been set to true to place the secondary axis on the opposite side.

Here is the complete code snippet for creating the chart

C#

```csharp
public class WeatherChartDemo:ContentPage
{
	public WeatherChartDemo()
	{
		//Initializing chart
		SfChartchart=newSfChart();
		chart.Title=newChartTitle(){Text="WeatherAnalysis"};

		//Initializing Primary Axis
		CategoryAxisprimaryAxis=newCategoryAxis();
		primaryAxis.Title=newChartAxisTitle(){Text="Month"};
		chart.PrimaryAxis=primaryAxis;

		//Initializing Secondary Axis
		NumericalAxissecondaryAxis=newNumericalAxis();
		secondaryAxis.Title=newChartAxisTitle(){Text="Temperature"};
		chart.SecondaryAxis=secondaryAxis;
		DataModeldataModel=newDataModel();

		//Adding Column Series to the chart for displaying precipitation
		chart.Series.Add(newColumnSeries(){
			ItemsSource=dataModel.Precipitation,
			Label="Precipitation",
			YAxis=newNumericalAxis(){OpposedPosition=true,
			ShowMajorGridLines = false}
		});

		//Adding the Spline Series to the chart for displaying high temperature
		chart.Series.Add(newSplineSeries(){
			ItemsSource=dataModel.HighTemperature,
			Label="High"
		});

		//Adding the Spline Series to the chart for displaying low temperature
		chart.Series.Add(newSplineSeries(){
			ItemsSource=dataModel.LowTemperature,
			Label="Low"
		});

		//Adding Chart Legend for the Chart
			chart.Legend=newChartLegend();
			his.Content=chart;
		}
	}

	public class DataModel
	{
		public ObservableCollectionHighTemperature;
		public ObservableCollectionLowTemperature;
		public ObservableCollectionPrecipitation;

		public DataModel()
		{
			HighTemperature=newObservableCollection();
			HighTemperature.Add(newChartDataPoint("Jan",42));
			HighTemperature.Add(newChartDataPoint("Feb",44));
			HighTemperature.Add(newChartDataPoint("Mar",53));
			HighTemperature.Add(newChartDataPoint("Apr",64));
			HighTemperature.Add(newChartDataPoint("May",75));
			HighTemperature.Add(newChartDataPoint("Jun",83));
			HighTemperature.Add(newChartDataPoint("Jul",87));
			HighTemperature.Add(newChartDataPoint("Aug",84));
			HighTemperature.Add(newChartDataPoint("Sep",78));
			HighTemperature.Add(newChartDataPoint("Oct",67));
			HighTemperature.Add(newChartDataPoint("Nov",55));
			HighTemperature.Add(newChartDataPoint("Dec",45));

			LowTemperature=newObservableCollection();
			LowTemperature.Add(newChartDataPoint("Jan",27));
			LowTemperature.Add(newChartDataPoint("Feb",28));
			LowTemperature.Add(newChartDataPoint("Mar",35));
			LowTemperature.Add(newChartDataPoint("Apr",44));
			LowTemperature.Add(newChartDataPoint("May",54));
			LowTemperature.Add(newChartDataPoint("Jun",63));
			LowTemperature.Add(newChartDataPoint("Jul",68));
			LowTemperature.Add(newChartDataPoint("Aug",66));
			LowTemperature.Add(newChartDataPoint("Sep",59));
			LowTemperature.Add(newChartDataPoint("Oct",48));
			LowTemperature.Add(newChartDataPoint("Nov",38));
			LowTemperature.Add(newChartDataPoint("Dec",29));

			Precipitation=newObservableCollection();
			Precipitation.Add(newChartDataPoint("Jan",3.03));
			Precipitation.Add(newChartDataPoint("Feb",2.48));
			Precipitation.Add(newChartDataPoint("Mar",3.23));
			Precipitation.Add(newChartDataPoint("Apr",3.15));
			Precipitation.Add(newChartDataPoint("May",4.13));
			Precipitation.Add(newChartDataPoint("Jun",3.23));
			Precipitation.Add(newChartDataPoint("Jul",4.13));
			Precipitation.Add(newChartDataPoint("Aug",4.88));
			Precipitation.Add(newChartDataPoint("Sep",3.82));
			Precipitation.Add(newChartDataPoint("Oct",3.07));
			Precipitation.Add(newChartDataPoint("Nov",2.83));
			Precipitation.Add(newChartDataPoint("Dec",2.8));
		}
	}
}
```

XAML

```xml
<chart:SfChart>
	<chart:SfChart.Legend>
		<chart:ChartLegend/>
	</chart:SfChart.Legend>

	<chart:SfChart.Title>
		<chart:ChartTitle Text="Weather Analysis"/>
	</chart:SfChart.Title>

	<chart:SfChart.PrimaryAxis>
		<chart:CategoryAxis>
			<chart:CategoryAxis.Title>
				<chart:ChartAxisTitle Text="Month"/>
			</chart:CategoryAxis.Title>
		</chart:CategoryAxis>
	</chart:SfChart.PrimaryAxis>
	<chart:SfChart.SecondaryAxis>
		<chart:NumericalAxis>
			<chart:NumericalAxis.Title>
				<chart:ChartAxisTitle Text="Month"/>
			</chart:NumericalAxis.Title>
		</chart:NumericalAxis>
	</chart:SfChart.SecondaryAxis>

	<chart:SfChart.Series>
		<chart:ColumnSeries   Label = "Low" ItemsSource = "{Binding Precipitation}">
			<chart:ColumnSeries.YAxis>
				<chart:NumericalAxis OpposedPosition="true"
					ShowMajorGridLines="false"/>
			</chart:ColumnSeries.YAxis>
		</chart:ColumnSeries>
		<chart:SplineSeries  Label = "High" ItemsSource =
			"{Binding HighTemperature}"/>
		<chart:SplineSeries  Label = "Low" ItemsSource =
			"{Binding LowTemperature}"/>
	</chart:SfChart.Series>
</chart:SfChart>
```
	
![chart](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/chart/gettingstarted/weatheranalysis.png)

#<a id="Gauge"></a>Gauge

**Introduction**

This section provides a quick overview for working with Essential Gauge for Xamarin.Forms. We will walk through the entire process of configuring a real world gauge.
You can also download the entire source code of this demo from [here](http://files2.syncfusion.com/Installs/v12.2.0.40/Samples/Xamarin/Gauge_GettingStarted.zip).

**Referencing Essential Studio components in your solution**

If you had acquired Essential Studio components through the Xamarin component store interface from within your IDE, then after adding the components to your Xamarin.iOS, Xamarin.Android and Windows Phone projects through the Component manager,  you will still need to manually reference the PCL (Portable Class Library) assemblies in the Xamarin.Forms PCL project in your solution. You can do this by manually adding the relevant PCL assembly references to your PCL project contained in the following path inside of your solution folder  

```
Components/syncfusionessentialstudio-version/lib/pcl/
```

Alternatively if you had downloaded Essential Studio from Syncfusion.com or through the Xamarin store web interface then all assembly references need to be added manually.  

After installing Essential Studio for Xamarin, all the required assemblies can be found in the installation folders, typically
	
**{Syncfusion Installed location}\Essential Studio\12.2.0.40\lib**

_Eg: C:\Program Files (x86)\Syncfusion\Essential Studio\12.2.0.40\lib_

or after downloading through the Xamarin store web interface, all the required assemblies can be found in the below folder

**{download location}\syncfusionessentialstudio-version\lib**

You can then add the assembly references to the respective projects as shown below

**PCL project**

```
pcl\Syncfusion.SfGauge.XForms.dll 
```

**Android project**

```
android\Syncfusion.SfGauge.Android.dll
android\Syncfusion.SfGauge.XForms.Android.dll
```

**iOS project**

```
ios\Syncfusion.SfGauge.iOS.dll 
ios\Syncfusion.SfGauge.XForms.iOS.dll
ios\Syncfusion.SfGauge.XForms.dll
```

**Windows Phone project**

```
wp8\Syncfusion.SfGauge.WP8.dll
wp8\Syncfusion.SfGauge.XForms.WinPhone.dll
```

Note: Essential Gauge for Xamarin is compatible with Xamarin.Forms v1.2.3.6257.

Currently an additional step is required for Windows Phone and iOS projects. We need to create an instance of the Gauge custom renderer as shown below.

Create an instance of SfGaugeRenderer in the MainPage constructor of the Windows Phone project as shown below

```csharp
public MainPage()
{
	new SfGaugeRenderer ();
	...  
}
```

Create an instance of SfGaugeRenderer in the FinishedLaunching overridden method of the AppDelegate class in the iOS Project as shown below

```csharp
public override bool FinishedLaunching(UIApplication app, NSDictionary options)
{
	...
	new SfGaugeRenderer ();
	...
}
```

**Adding and configuring the gauge**

The gauge control can be configured entirely in C# code or using XAML markup.

Create an instance of SfCircularGauge

C#

```csharp

   SfCircularGauge circularGauge = new SfCircularGauge();

```

XAML

```xml

<?xml version="1.0" encoding="UTF-8"?>
<ContentPage xmlns=http://xamarin.com/schemas/2014/forms
	xmlns:local="clr-
	namespace:Syncfusion.SfGauge.XForms;assembly=Syncfusion.SfGauge.XForms"
	xmlns:x=http://schemas.microsoft.com/winfx/2009/xaml
	x:Class="GaugeGettingStarted.Sample">
	<ContentPage.Content>
		<local:SfCircularGauge>
		</local:SfCircularGauge>
	</ContentPage.Content>
</ContentPage>
```
	
**Insert a Scale**

The next step is to add one of more scales.

C#

```csharp
SfCircularGauge circularGauge = new SfCircularGauge();

// Add a scale

Scale scale = new Scale();
scale.StartValue= 0;
scale.EndValue =100;
scale.Interval = 10;
scale.StartAngle =135;
scale.SweepAngle =270;
scale.RimThickness = 10;
scale.RimColor = Color.FromHex("#FFFB0101");
scale.MinorTicksPerInterval = 3;

circularGauge.Scales.Add(scale);
```

XAML

```xml
<gauge:SfCircularGauge>
	<gauge.SfCircularGauge.Scales>
		<gauge.Scale StartValue="0"
			EndValue="100" Interval="10"
			StartAngle="135" SweepAngle="230"
			RimColor="#FFFB0101" RimThickness="10"  />
	</gauge.SfCircularGauge.Scales>
</gauge:SfCircularGauge>
```

**Specify Ranges**

We can improve the readability of data by including ranges that quickly show when values fall within specific ranges.

C#

```csharp
... 
Range range = new Range();
range.StartValue = 0;
range.EndValue = 80;
range.Color = Color.FromHex("#FF777777");
range.Thickness = 10;

scale.Ranges.Add(range);
```

XAML

```xml
<local:SfCircularGauge>
	<local:SfCircularLocal:Scales>
		<local:Scale StartValue="0" EndValue="100"
			Interval="10" StartAngle="135"
			SweepAngle="230" RimColor="#FFFB0101"
			RimThickness="10" >
			<local:Scale.Ranges>
				<local:Range StartValue="0" EndValue="80" Color="#FF777777" Thickness="15" />
			</local:Scale.Ranges>
		</local:Scale>
	</local:SfCircularLocal:Scales>
</local:SfCircularGauge>
```

**Add a Needle Pointer**

We will now create a needle pointer and associate it with a scale to display the current value.

C#

```csharp
NeedlePointer needlePointer = new NeedlePointer();
needlePointer.Value = 60;
needlePointer.Color = Color.White;
needlePointer.KnobColor = Color.White;
needlePointer.Thickness = 5;
needlePointer.KnobRadius = 20;
needlePointer.LengthFactor = 0.8;

scale.Pointers.Add(needlePointer);
...
```

XAML

```xml
<local:SfCircularGauge>
	<local:SfCircularLocal:Scales>
		<local:Scale StartValue="0" EndValue="100"
			Interval="10" StartAngle="135"
			SweepAngle="230" RimColor="#FFFB0101"
			RimThickness="10" >
			<local:Scale.Ranges>
				<local:Range StartValue="0" EndValue="80" Color="#FF777777" Thickness="15" />
			</local:Scale.Ranges>
			<local:Scale.Pointers>
				<local:NeedlePointer Value="60" LengthFactor="0.8"
					Color="White" Thickness="5"
					KnobColor="White" KnobRadius="20"  />
			</local:Scale.Pointers>
		</local:Scale>
	</local:SfCircularLocal:Scales>
</local:SfCircularGauge>
```

**Add a Range Pointer**

A range pointer provides an alternative way of indicating the current value.

C#

```csharp
RangePointer rangePointer = new RangePointer();
rangePointer.Value = 60;
rangePointer.Color = Color.FromHex("#FFA9A9A9");
rangePointer.Thickness = 10;

scale.Pointers.Add(rangePointer);
...
```

XAML

```xml
<local:SfCircularGauge>
	<local:SfCircularLocal:Scales>
		<local:Scale StartValue="0" EndValue="100"
			Interval="10" StartAngle="135"
			SweepAngle="230" RimColor="#FFFB0101"
			RimThickness="10" >
			<local:Scale.Ranges>
				<local:Range StartValue="0" EndValue="80" Color="#FF777777" Thickness="15" />
			</local:Scale.Ranges>
			<local:Scale.Pointers>
				<local:NeedlePointer Value="60" LengthFactor="0.8"
					Color="White" Thickness="5"
					KnobColor="White" KnobRadius="20"  />
				<local:RangePointer Value="60" Color="White"
						Thickness="10" />
			</local:Scale.Pointers>
		</local:Scale>
	</local:SfCircularLocal:Scales>
</local:SfCircularGauge>
```

![gauge](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/gauge/getting-started.png)

#<a id="TreeMap"></a>TreeMap

**Overview**

This section provides a quick overview for working with Essential Treemap for Xamarin.Forms. We will walk through the entire process of creating a real world Treemap.

The goal of this tutorial is to visualize population growth across continents.
You can also download the entire source code of this demo from [here](http://files2.syncfusion.com/Installs/v12.2.0.40/Samples/Xamarin/TreeMap_GettingStarted.zip).

**Referencing Essential Studio components in your solution**

If you had acquired Essential Studio components through the Xamarin component store interface from within your IDE, then after adding the components to your Xamarin.iOS, Xamarin.Android and Windows Phone projects through the Component manager,  you will still need to manually reference the PCL (Portable Class Library) assemblies in the Xamarin.Forms PCL project in your solution. You can do this by manually adding the relevant PCL assembly references to your PCL project contained in the following path inside of your solution folder  

```
Components/syncfusionessentialstudio-version/lib/pcl/
```

Alternatively if you had downloaded Essential Studio from Syncfusion.com or through the Xamarin store web interface then all assembly references need to be added manually.  

After installing Essential Studio for Xamarin, all the required assemblies can be found in the installation folders, typically
	
**{Syncfusion Installed location}\Essential Studio\12.2.0.40\lib**

_Eg: C:\Program Files (x86)\Syncfusion\Essential Studio\12.2.0.40\lib_

or after downloading through the Xamarin store web interface, all the required assemblies can be found in the below folder

**{download location}\syncfusionessentialstudio-version\lib**

You can then add the assembly references to the respective projects as shown below

**PCL project**

```
pcl\Syncfusion.SfTreeMap.XForms.dll 
```

**Android project**

```
android\Syncfusion.SfTreeMap.Android.dll
android\Syncfusion.SfTreeMap. XForms.Android.dll
```

**iOS project**
```
ios\Syncfusion.SfTreeMap.iOS.dll 
ios\Syncfusion.SfTreeMap.XForms.iOS.dll
ios\Syncfusion.SfTreeMap.XForms.dll
```

**Windows Phone project**

```
wp8\Syncfusion.SfTreeMap.WP8.dll
wp8\Syncfusion.SfTreeMap.XForms.WinPhone.dll
```

Note: Essential TreeMap for Xamarin is compatible with Xamarin.Forms v1.2.3.6257.

Currently an additional step is required for Windows Phone and iOS projects. We need to create an instance of the TreeMap custom renderer as shown below.

Create an instance of SfTreeMapRenderer in the MainPage constructor of the Windows Phone project as shown below

```csharp
public MainPage()
{
	new SfTreeMapRenderer ();
     ...  
}
```

Create an instance of SfTreeMapRenderer in the FinishedLaunching overridden method of the AppDelegate class in iOS Project as shown below

```csharp
public override bool FinishedLaunching(UIApplication app, NSDictionary options)
{
	new SfTreeMapRenderer ();
	...  
}
```

**Initializing the TreeMap**

The Treemap control can be configured entirely in C# code or using XAML markup.

The first step is to create a TreeMap object

```xml
<?xml version="1.0" encoding="UTF-8"?>
<ContentPage xmlns=http://xamarin.com/schemas/2014/forms
xmlns:local="clr-
namespace:Syncfusion.SfTreeMap.XForms;assembly=Syncfusion.SfTreeMap.XForms"
xmlns:x=http://schemas.microsoft.com/winfx/2009/xaml
x:Class="TreeMapGettingStarted.Sample" BackgroundColor=îBlackî>
	<ContentPage.Content >
		<local:SfTreeMap x:Name="treeMap">
		</local:SfTreeMap>
	</ContentPage.Content>
</ContentPage>
```

```csharp
public static Page GetMainPage()
{
        SfTreeMap treeMap = new SfTreeMap();
        return new ContentPage {
		BackgroundColor = Color.Black,
		Content = treeMap,
	};
}
```

**Populating TreeMap Items**

The TreeMap accepts a collection of TreeMapItems as input.

XAML

```xml
// BindingContext is set for the content page class.
// DataModel model = new DataModel();
//..
//..
//this.BindingContext = model;

<local:SfTreeMap x:Name="treeMap" Items = "{Binding TreeMapItems}">
</local:SfTreeMap>    
```

C#
```csharp
public class DataModel : BindableObject
{
	public static readonly BindableProperty TreeMapItemsProperty =
		BindableProperty.Create>(p =>
			p.TreeMapItems, null, BindingMode.TwoWay, null, null, null, null);

	public ObservableCollection TreeMapItems
	{
		get { return (ObservableCollection)GetValue(TreeMapItemsProperty); }
		set { SetValue(TreeMapItemsProperty, value); }
	}

	public DataModel()
	{
		this.TreeMapItems = new ObservableCollection();

		TreeMapItems.Add(newTreeMapItem() { Label = "Indonesia", ColorWeight = 3,Weight = 237641326 });
		TreeMapItems.Add(newTreeMapItem() { Label = "Russia", ColorWeight = 2, Weight = 152518015 });
		TreeMapItems.Add(newTreeMapItem() { Label = "United States", ColorWeight = 4, Weight = 315645000 });
		TreeMapItems.Add(newTreeMapItem() { Label = "Mexico", ColorWeight = 2, Weight = 112336538 });
		TreeMapItems.Add(newTreeMapItem() { Label = "Nigeria", ColorWeight = 2, Weight = 170901000 });
		TreeMapItems.Add(newTreeMapItem() { Label = "Egypt", ColorWeight = 1, Weight = 83661000 });
		TreeMapItems.Add(newTreeMapItem() { Label = "Germany", ColorWeight = 1, Weight = 81993000 });
		TreeMapItems.Add(newTreeMapItem() { Label = "France", ColorWeight = 1, Weight = 65605000 });
		TreeMapItems.Add(newTreeMapItem() { Label = "UK", ColorWeight = 1, Weight = 63181775 });
	}
}

SfTreeMap treeMap = new SfTreeMap();
DataModel model = new DataModel();
treeMap.Items = model.TreeMapItems;
```

Grouping of TreeMap Items using Levels

You can group TreeMap Items using two types of levels

1. TreeMap Flat Level
2. TreeMap Hierarchical Level


**Customizing TreeMap Appearance using ranges**

Fill colors for value ranges can be specified using From and To properties.

XAML

```xml
<local:SfTreeMap x:Name="treeMap" Items = "{Binding TreeMapItems}">
	<local:SfTreeMap.LeafItemColorMapping>
		<local:RangeColorMapping>
			<local:RangeColorMapping.Ranges>
				<local:Range LegendLabel = "1 % Growth" From = "0" To = "1" Color =  "#77D8D8"  />
				<local:Range LegendLabel = "2 % Growth" From = "0" To = "2" Color =  "#AED960"  />
				<local:Range LegendLabel = "3 % Growth" From = "0" To = "3" Color =  "#FFAF51"  />
				<local:Range LegendLabel = "4 % Growth" From = "0" To = "4" Color =  "#F3D240"  />
			</local:RangeColorMapping.Ranges>
		</local:RangeColorMapping>
	</local:SfTreeMap.LeafItemColorMapping>
</local:SfTreeMap>
```

C#

```csharp
... 
ObservableCollection ranges = new ObservableCollection();
ranges.Add(newRange() { LegendLabel="1 % Growth", From = 0, To = 1, Color = Color.FromHex("#77D8D8") });
ranges.Add(newRange() { LegendLabel = "2 % Growth", From = 0, To = 2, Color = Color.FromHex("#AED960") });
ranges.Add(newRange() { LegendLabel = "3 % Growth", From = 0, To = 3, Color = Color.FromHex("#FFAF51") });
ranges.Add(newRange() { LegendLabel = "4 % Growth", From = 0, To = 4, Color = Color.FromHex("#F3D240") });

treeMap.LeafItemColorMapping = new RangeColorMapping (){ Ranges = ranges };
```

**Leaf level TreeMap item customization**

The Leaf level TreeMap items can be customized using LeafItem Setting.

XAML

```xml
<local:SfTreeMap x:Name="treeMap" Items = "{Binding TreeMapItems}">
	<!-- ... -->
	<local:SfTreeMap.LeafItemSettings>
		<local: LeafItemSettings BorderWidth="1" BorderColor="White"  />
	</local:SfTreeMap.LeafItemSettings >
</local:SfTreeMap>
```

C#

```csharp
...
LeafItemSettings leafSetting = new LeafItemSettings();
leafSetting.BorderWidth = 1;
leafSetting.BorderColor = Color.White;
    
treeMap.LeafItemSettings = leafSetting;
```

**Enable Legend**

Displaying legends if only appropriate for TreeMaps whose leaf nodes have been colored using RangeColorMapping. You can set ShowLegend property value to "True"to make the legend visible.

**Label for Legends**

You can customize the labels of the legend items using the LegendLabel property of RangeColorMapping.

XAML

```xml
<local:SfTreeMap.LegendSettings>
	<local:LegendSettings  ShowLegend ="true" IconSize="15,15" Size="350,70"  />
</local:SfTreeMap.LegendSettings>
```

C#

```csharp
...
LegendSettings legendSettings = new LegendSettings();
legendSettings.ShowLegend= true;
legendSettings.IconSize = new Size(15, 15);
legendSettings.Size = new Size (350, 70);

treeMap.LegendSettings= legendSettings;
```

![treemap](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/tree-map/getting-started/treemap.png)

#<a id="XlsIO"></a>XlsIO

Essential XlsIO for Xamarin is a .NET PCL library that can be used to create and modify Microsoft Excel documents from within Xamarin.iOS, Xamarin.Android and Xamarin.Forms applications.

**Supported Features**

- Charts for data visualization
- Conditional Formatting
- Data Validation
- Tables
- Importing XML data
- Importing Business Objects
- Formulas
- Template Marker
- Auto-Shapes
- Cells Formatting
- Cell Grouping
- Data Filtering
- Data Sorting
- Find Data
- Comments
- HTML Conversion
- Named Ranges
- Number Formats
- Page settings
- Page breaks
- Header and footer images
- R1C1 Reference Mode
- Re-calculation
- R1C1 Formulas
- Dis-contiguous Named Ranges
- Hyperlinks
- Freeze panes
- Sheet Tab color RGB
- Hide rows and columns

**Getting Started**

The following steps demonstrate how to create a simple excel document in a Xamarin.Forms project using Essential XlsIO. You can also download the entire source code of this demo from [here](http://files2.syncfusion.com/Installs/v12.2.0.40/Samples/Xamarin/XlsIO_GettingStarted.zip).

1. Create new Xamarin.Forms portable project.

2. Next, you need to reference Essential Studio components in your solution. 
   XlsIO is packaged as a portable class library so currently there is no way to add reference to it from within the Xamarin component store IDE interface. You would need to obtain the required assemblies either through the Xamarin component store  web interface or from Syncfusion.com. Typically you would add reference to XlsIO only in the PCL project of your Xamarin.Forms application. The required assembly references are

	```
	Syncfusion.Compression.Portable.dll
	Syncfusion.XlsIO.Portable.dll
	```
	Note: If you had already referenced one of our UI components (Chart, Gauge or TreeMap) components from within the Xamarin component store IDE interface then the XlsIO assembly has already been downloaded and available in your solution folder, You can then manually add references from that folder.

	```
	Components/syncfusionessentialstudio-version/lib/pcl/
	```		

3. Use the following C# code to generate a simple excel file using Essential XlsIO

	```csharp
	//Instantiate excel engine
	ExcelEngine excelEngine = new ExcelEngine();

	//Excel application
	IApplication application = excelEngine.Excel;
	application.DefaultVersion = ExcelVersion.Excel2013;

	//A new workbook is created.[Equivalent to creating a new workbook in MS Excel]
	//The new workbook will have 1 worksheet
	IWorkbook workbook = application.Workbooks.Create(1);

	//The first worksheet object in the worksheets collection is accessed.
	IWorksheet sheet = workbook.Worksheets[0];
	sheet.Range["A1"].Text = "Hello World!";
	workbook.Version = ExcelVersion.Excel2013;

	//Saving workbook as stream
	MemoryStream stream = new MemoryStream();
	workbook.SaveAs(stream);

	//Closing workbook
	workbook.Close();

	//Disposing excel engine
	excelEngine.Dispose();
	```

**Screenshots**

![XlsIO]( http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/XlsIO/gettingstarted.png )

#<a id="DocIO"></a>DocIO

Essential DocIO for Xamarin is a .NET PCL library that can be used to read and write Microsoft Word documents from within Xamarin.iOS, Xamarin.Android and Xamarin.Forms applications.

**Features**

Here is a quick summary of the features available in Essential DocIO

- Create new Word documents.
- Modify existing Microsoft Word documents.
- Format text, tables using built-in and custom styles.
- Insert bullets and numbering.
- Insert, edit, and remove fields, form fields, hyperlinks, endnotes, footnotes, comments, Header footers.
- Insert and extract images, OLE objects.
- Insert line breaks, page breaks, column breaks and section breaks.
- Find and Replace text with its original formatting.
- Insert Bookmarks and navigate corresponding bookmarks to insert, replace, and delete content.
- Advanced Mail Merge support with different data sources.
- Clone multiple documents and merge into a single document.
- Read and write Built-In and Custom Document Properties.
- Define page setup settings and background.
- Create or edit Word 97-2003, 2007, 2010, and 2013 documents

**Getting Started**

The following steps demonstrate how to create a simple word document in a Xamarin.Forms project using Essential DocIO.
You can also download the entire source code of this demo from [here](http://files2.syncfusion.com/Installs/v12.2.0.40/Samples/Xamarin/DocIO_GettingStarted.zip).

1. Create new Xamarin.Forms portable project.

2. Next, you need to reference Essential Studio components in your solution. 
   DocIO is packaged as a portable class library so currently there is no way to add reference to it from within the Xamarin component store IDE interface. You would need to obtain the required assemblies either through the Xamarin component store  web interface or from Syncfusion.com. Typically you would add reference to DocIO only in the PCL project of your application. The required assembly references are

	```
	Syncfusion.Compression.Portable.dll
	Syncfusion.DocIO.Portable.dll
	```
	If you had already referenced one of our UI components (Chart, Gauge or Treemap) components from within the Xamarin component store IDE interface then the DocIO assembly has already been downloaded and available in your solution folder, You can then manually add references from that folder.

	```
	Components/syncfusionessentialstudio-version/lib/pcl/
	```

3. A new Word document can be easily created from scratch by instantiating a new instance of the WordDocument class. This class is the root node for all other nodes in the Document Object Model for Essential DocIO library. Using this DOM, you can add, edit, and remove content from documents by iterating elements. The following code example illustrates how to create a Word document with minimal content (one section and one paragraph).

	```csharp
	//Creates a new Word document instance
	WordDocument doc = new WordDocument();

	//Adds one section and one paragraph to the document.
	doc.EnsureMinimal();
	```

4. Add a new section at the end of a document by invoking the AddSection method of WordDocument class. The following code example illustrates how to add a new section to a Word document.

	```csharp
	//Adds a new section to the document.
	WSection section = doc.AddSection();
	```

5. Add a new paragraph at the end of section by invoking the AddParagraph method of WSection class; also, you can add a new table at the end of section by invoking the AddTable method of WSection class. The following code example illustrates how to add a new Paragraph and Table to a Word document.

	```csharp
	//Adds a new Paragraph to the section.
	IWParagraph para = section.AddParagraph();

	//Adds a new Table to the section.
	IWTable table = section.AddTable();
	```

6. You can append text to a paragraph by invoking the AppendText method of WParagraph class. The following code example illustrates how to append text to a Word document.

	```csharp
	//Appends text to the paragraph.
	paragraph.AppendText("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
	```

7. You can save a Word document by invoking the Save method of WordDocument class. The following code example illustrates how to save a Word document.

	```csharp
	//Saves the generated Word document.
	MemoryStream stream = new MemoryStream();
	doc.Save(stream, FormatType.Word2013);

	//Releases the resources used by the WordDocument instance.
	doc.Close();
	```


**Screenshots**

![DocIO]( http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/DocIO/gettingstarted.png )



#<a id="PDF"></a>PDF


Essential PDF for Xamarin is a .NET PCL library that can be used to create and modify Adobe PDF documents from within Xamarin.iOS, Xamarin.Android and Xamarin.Forms applications.

All of the elements in a typical PDF file like the text, formatting, images and tables are accessible through a comprehensive set of API's. This makes it possible to easily create richly formatted PDF documents as well as modify existing ones.

**Features:**

**Document level features:**

- Create and load PDF documents files and streams
- Save PDF files to disk and Streams
- Document information
- Document viewer preference
- Document file attachments
- Document level java scripts and actions
- Document outline
- Add and remove Pdf pages
- Import page form existing document
- Document merging 
- Booklet
- Stamp
- Page orientation
- Page sizes
- Headers and Footers
- Actions

**Text**

- Drawing Text
- Text formatting
- Pagination

**Graphics**

- Pen and brush for stroking operations
- Graphics primitives: lines, ellipses, rectangles, arcs, pie, Bezier curves, paths.
- Layers
- Patterns
- Drawing of external page content
- Color spaces
- Barcode

**Forms**

- Create, load and save PDF forms
- Add, edit, remove and rename form fields
- Supporting text box fields, combo box fields, list box fields, push button fields, radio button fields
- Flatten form fields
- Enumerating the form fields
- Form actions

**Fonts**

- Standard Fonts

**Images**

- Jpeg image support

**Tables** :

- Cell/Row/Column formatting
- Header
- Pagination
- Borders
- Row span and column span
- Nested
- Cell Padding and spacing

**Page Level Operations**

- Headers and Footers
- Page Label
- Automatic fields

**Pdf Annotations**

- Add, edit and remove pdf annotations
- Custom appearance for annotations

**Supported annotations**

- Free Text annotation
- Rubber stamp annotations
- File attachment annotation
- Link annotation
- Line annotation
- Ink annotations
- Text markup annotations
- sound annotations
- 3D-Annotations.

**Barcode**

- Add the barcode into the PDF document

**Supported barcodes:**

- QR barcode
- Data matrix barcode
- Code39
- Code39ext
- Code 11
- Coda bar
- Code32
- Code93
- Code93 extended
- Code128 A
- Code128 B
- Code128 C

**Getting Started:**

The following steps demonstrate how create a simple PDF document in a Xamarin.Forms project using Essential PDF. You can also download the entire source code of this demo from [here](http://files2.syncfusion.com/Installs/v12.2.0.40/Samples/Xamarin/PDF_GettingStarted.zip).

1. Create a new Xamarin.Forms portable project.

2. Next, you need to reference Essential Studio components in your solution. 
   Essential PDF is packaged as a portable class library so currently there is no way to add reference to it from within the Xamarin component store IDE interface. You would need to obtain the required assemblies either through the Xamarin component store  web interface or from Syncfusion.com. Typically you would add reference to Essential PDF only in the PCL project of your application. The required assembly references are

	```
	Syncfusion.Compression.Portable.dll
	Syncfusion.Pdf.Portable.dll
	```
	Note: If you had already referenced one of our UI components (Chart, Gauge or Treemap) components from within the Xamarin component store IDE interface then the DocIO assembly has already been downloaded and available in your solution folder, You can then manually add references from that folder.

	```
	Components/syncfusionessentialstudio-version/lib/pcl/
	```

3. Use the following C# code to generate a simple PDF using Essential PDF

	```csharp
        //Create a new PDF document.
        PdfDocument document = new PdfDocument();

        //Add a page
        PdfPage page = document.Pages.Add();

        //Creates Pdf graphics for the page
        PdfGraphics graphics = page.Graphics;

        //Creates a solid brush.
        PdfBrushbrush =newPdfSolidBrush(Color.Black);

        //Sets the font.
        PdfFontfont =newPdfStandardFont(PdfFontFamily.Helvetica, fontSize);

        //Draws the text.
        graphics.DrawString("Lorem Ipsum is simply dummy text of the
		printing and typesetting industry. Lorem Ipsum has been the
		standard dummy text ever since the 1500s, when an unknown printer
		took a galley of type and scrambled it to make a type specimen
		book.", font, brush, new RectangleF(0,0, page.GetClientSize().
		Width,200));

        //Saves the document.
        MemoryStream stream = new MemoryStream();
        document.Save(stream);
        document.Close(true);
	```

**Screenshots**

![Pdf]( http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/Pdf/gettingstarted.png )