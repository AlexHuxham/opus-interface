**Overview**

Essential Studio for Xamarin contains all the necessary components needed to build typical line of business mobile applications that run on iOS, Android and Windows Phone. User interface elements like the charts and gauges can be configured in a common view using C# or XAML and it gets rendered as native controls on the respective platforms without any additional work.

This suite also includes a powerful set of components for manipulating Microsoft Excel, Microsoft Word and Adobe PDF file formats. All the file format components have a feature rich API that exposes most of the functionality that the underlying file format itself has to offer.

Here is the complete list of all the components that ship with Essential Studio for Xamarin

**Data Visualization**

  [__Chart__](#Chart)

  [__Gauge__](#Gauge)

  [__TreeMap__](#TreeMap)

**File Format**

  [__XlsIO__](#XlsIO)

  [__DocIO__](#DocIO)

  [__PDF__](#PDF)



#<a id="Chart"></a>Chart

Essential Chart for Xamarin.Forms is a visually stunning charting component that is also easy to use. It includes all the common chart types ranging from line charts to financial charts.

**Chart Types**

14 chart types can be plotted including Line, Spline, Column, Area, Bar, Pie, Candle, OHLC, Stacking Column, Stacking Bar, Stacking Area, Stacking Column 100, Stacking Bar 100 and Stacking Area 100.

**Area Series**

![chart]( http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/chart/area.png)

**Column Series**

![chart](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/chart/column.png)

**Bar Series**

![chart](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/chart/bar.png)

**Line Series**

![chart](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/chart/line.png)

**Spline Series**

![chart](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/chart/spline.png)

**Pie Series**

![chart](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/chart/pie.png)

**Stacking Area Series**

![chart](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/chart/stackingarea.png)

**Stacking Area 100 Series**

![chart](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/chart/stackingarea100.png)

**Stacking Bar Series**

![chart](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/chart/stackingbar.png)

**Stacking Bar 100 Series**

![chart](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/chart/stackingbar100.png)

**Stacking Column Series**

![chart](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/chart/stackingcolumn.png)

**Stacking Column 100 Series**

![chart](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/chart/stackingcolumn100.png)

**Candle Series**

![chart](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/chart/candle.png)

**OHLC Series**

![chart](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/chart/ohlc.png)

**Axis**

Essential Chart has several specialized axis types including NumericalAxis, CategoryAxis and DateTimeAxis. Several aspects of the axis like position, labels appearance, gridlines and ticks can be customized.

**Numeric Axis**

![chart](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/chart/numericaxis.png)

**Category Axis**

![chart](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/chart/categoryaxis.png)

**Date Time Axis**

![chart](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/chart/datetimeaxis.png)

**Flexible label positioning**

The position of axis labels can be fully customized to provide better readability and to avoid any potential overlap.

![chart](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/chart/labelrotation.png)

**Automatic range calculation with range padding**

Optimal ranges are automatically calculated based on the given data; the calculated ranges can also be customized using range padding.

**Range Padding � None**

![chart](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/chart/rangepaddingnone.png)

**Range Padding - Round**

![chart](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/chart/rangepaddinground.png)

**Range Padding - Additional**

![chart](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/chart/rangepaddingadditional.png)

**Multiple Axes**

Data can be plotted against multiple scales in a single chart.

![chart](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/chart/multipleaxis.png)

**Interactive Legends**

Legends can be positioned anywhere within the chart area. It is also possible to toggle the visibility of specific series by tapping on the legend items. This functionality is very useful when the user needs to focus on the data in specific series only.

![chart](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/chart/legend.png)

**High Performance**

Essential Chart has been built from the ground up to handle large volumes of data. It can easily render thousands of data points within a second.

![chart](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/chart/performance.png)

#<a id="Gauge"></a>Gauge

The Gauge control for Xamarin.Forms lets you visualize numeric values over a circular scale. The appearance of the gauge can be fully customized to seamlessly integrate with your applications.

**Range Indicators**

Range indicators help quickly visualize the range within which a value falls on a scale. This functionality is very useful when needing to quickly determine if a value has crossed a certain threshold.

| ![Gauge](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/gauge/ft-radial-indicator-1.png)                      |
| --- |
| 

**Customizable Frames**

The Gauge control can be rendered within circular, semi-circular, and quadrant frames so it fits within the available space.

![Gauge](http://www.syncfusion.com//content/en-US/Products/Images/Xamarin/gauge/ft-customizable-frame-1.png)

**Needle Pointer**

The current value can be indicated by using a fully customizable pointer.

| ![Gauge](http://www.syncfusion.com//content/en-US/Products/Images/Xamarin/gauge/ft-needle-pointer-1.png)            |
| --- |

**Range Pointer**

The range pointer can be alternatively used in place of the needle pointer

![Gauge](http://www.syncfusion.com//content/en-US/Products/Images/Xamarin/gauge/ft-range-pointer-1.png)

|  |
| --- |
|  |

#<a id="TreeMap"></a>TreeMap

The TreeMap control for Xamarin.Forms provides a simple but effective way to visualize flat or hierarchical data as clustered rectangles with proportions determined by data values.

**High Performance**

The TreeMap control is optimized to visualize large amounts of flat or hierarchical data.

![TreeMap](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/tree-map/large-data.png)

**Grouping**

Data can be grouped to provide a top-level view.

![TreeMap](http://www.syncfusion.com/content/en-us/products/images/Xamarin/tree-map/grouping.png)

**Rich Interactivity**

Readability of the data can be greatly enhanced using interactive legends and labels.

![TreeMap](http://www.syncfusion.com/content/en-us/products/images/Xamarin/tree-map/tree-map-elements.png)

**Layout Options**

Visualize data using one of the available layout algorithms, such as **Squarified** , **SliceAndDiceAuto** , **SliceAndDiceHorizontal** , or **SliceAndDiceVertical** to depict the relative weight of hierarchical data relationships at more than one level.

![TreeMap](http://www.syncfusion.com/content/en-us/products/images/Xamarin/tree-map/ft-squarified-1.png)

![TreeMap](http://www.syncfusion.com/content/en-us/products/images/Xamarin/tree-map/ft-sliceanddiceauto-1.png)

![TreeMap](http://www.syncfusion.com/content/en-us/products/images/Xamarin/tree-map/ft-sliceanddicehorizontal-1.png)

![TreeMap](http://www.syncfusion.com/content/en-us/products/images/Xamarin/tree-map/ft-sliceanddicevertical-1.png)

#<a id="XlsIO"></a>XlsIO

Essential XlsIO for Xamarin is a .NET PCL library that can be used to create and modify Microsoft Excel documents from within Xamarin.iOS, Xamarin.Android and Xamarin.Forms applications.

**Supported Features**

- Charts for data visualization
- Conditional Formatting
- Data Validation
- Tables
- Importing XML data
- Importing Business Objects
- Formulas
- Template Marker
- Auto-Shapes
- Cells Formatting
- Cell Grouping
- Data Filtering
- Data Sorting
- Find Data
- Comments
- HTML Conversion
- Named Ranges
- Number Formats
- Page settings
- Page breaks
- Header and footer images
- R1C1 Reference Mode
- Re-calculation
- R1C1 Formulas
- Dis-contiguous Named Ranges
- Hyperlinks
- Freeze panes
- Sheet Tab color RGB
- Hide rows and columns

![XlsIO](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/XlsIO/excel_invoice.png)

#<a id="DocIO"></a>DocIO

Essential DocIO for Xamarin is a .NET PCL library that can be used to read and write Microsoft Word documents from within Xamarin.iOS, Xamarin.Android and Xamarin.Forms applications.

**Features**

Here is a quick summary of the features available in Essential DocIO

- Create new Word documents.
- Modify existing Microsoft Word documents.
- Format text, tables using built-in and custom styles.
- Insert bullets and numbering.
- Insert, edit, and remove fields, form fields, hyperlinks, endnotes, footnotes, comments, Header footers.
- Insert and extract images, OLE objects.
- Insert line breaks, page breaks, column breaks and section breaks.
- Find and Replace text with its original formatting.
- Insert Bookmarks and navigate corresponding bookmarks to insert, replace, and delete content.
- Advanced Mail Merge support with different data sources.
- Clone multiple documents and merge into a single document.
- Read and write Built-In and Custom Document Properties.
- Define page setup settings and background. 
- Create or edit Word 97-2003, 2007, 2010, and 2013 documents



![DocIO](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/DocIO/word_invoice.png)

#<a id="PDF"></a>PDF

Essential PDF for Xamarin is a .NET PCL library that can be used to create and modify Adobe PDF documents from within Xamarin.iOS, Xamarin.Android and Xamarin.Forms applications.

All of the elements in a typical PDF file like the text, formatting, images and tables are accessible through a comprehensive set of API's. This makes it possible to easily create richly formatted PDF documents as well as modify existing ones.

**Features:**

**Document level features:**

- Create and load PDF documents files and streams
- Save PDF files to disk and Streams
- Document information
- Document viewer preference
- Document file attachments
- Document level java scripts and actions
- Document outline
- Add and remove Pdf pages
- Import page form existing document
- Document merging 
- Booklet
- Stamp
- Page orientation
- Page sizes
- Headers and Footers
- Actions

**Text**

- Drawing Text
- Text formatting
- Pagination

**Graphics**

- Pen and brush for stroking operations
- Graphics primitives: lines, ellipses, rectangles, arcs, pie, Bezier curves, paths.
- Layers
- Patterns
- Drawing of external page content
- Color spaces
- Barcode

**Forms**

- Create, load and save PDF forms
- Add, edit, remove and rename form fields
- Supporting text box fields, combo box fields, list box fields, push button fields, radio button fields
- Flatten form fields
- Enumerating the form fields
- Form actions

**Fonts**

- Standard Fonts

**Images**

- Jpeg image support

**Tables:**

- Cell/Row/Column formatting
- Header
- Pagination
- Borders
- Row span and column span
- Nested
- Cell Padding and spacing

**Page Level Operations**

- Headers and Footers
- Page Label
- Automatic fields

**Pdf Annotations**

- Add, edit and remove pdf annotations
- Custom appearance for annotations

**Supported annotations**

- Free Text annotation
- Rubber stamp annotations
- File attachment annotation
- Link annotation
- Line annotation
- Ink annotations
- Text markup annotations
- sound annotations
- 3D-Annotations.

**Barcode**

- Add the barcode into the PDF document

**Supported barcodes:**

- QR barcode
- Data matrix barcode
- Code39
- Code39ext
- Code 11
- Coda bar
- Code32
- Code93
- Code93 extended
- Code128 A
- Code128 B
- Code128 C

![Pdf](http://www.syncfusion.com/Content/en-US/products/Images/Xamarin/Pdf/pdf_invoice.png)