﻿using System;
using System.Threading;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

using Xamarin.Forms.Platform.Android;


namespace MasterDetail.Android
{
	[Activity (Label = "OPUS", MainLauncher = true, Theme="@style/android:Theme.Holo.Light.NoActionBar", ConfigurationChanges = ConfigChanges.ScreenSize | 
		ConfigChanges.Orientation)]

	public class MainActivity : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			Thread.Sleep (3000);

			Xamarin.Forms.Forms.Init (this, bundle);

			StartActivity (typeof(MainActivity));
			//LoadApplication (new App ());

		}
	}
}

