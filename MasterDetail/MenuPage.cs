﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace MasterDetail
{
	public class MenuPage : ContentPage
	{
	
		public ListView Menu { get; set; }

		public MenuPage ()
		{
			Icon = "settings.png";
			Title = "MENU";
			BackgroundColor = Color.FromHex ("0096d7");
			//BackgroundColor = Color.Transparent;
			//BackgroundImage = "image.png";
													
			Menu = new MenuListView ();

			var menuLabel = new ContentView {
				Padding = new Thickness (10, 36, 0, 5),
				Content = new Label {
					TextColor = Color.FromHex ("000000"),
					Text = "MENU",
					FontFamily = "Avenir"
				}
			};

			var layout = new StackLayout { 
				Spacing = 0, 
				VerticalOptions = LayoutOptions.FillAndExpand,

			};

//			Device.OnPlatform (
//				iOS: () => ((Xamarin.Forms.Label)menuLabel.Content).FontSize = Font.SystemFontOfSize (NamedSize.Micro),
//				Android: () => ((Xamarin.Forms.Label)menuLabel.Content).FontSize = Font.SystemFontOfSize (NamedSize.Medium)
//			);

			layout.Children.Add (menuLabel);
			layout.Children.Add (Menu);

			Content = layout;
		}
	}
}