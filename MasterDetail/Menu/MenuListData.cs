using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace MasterDetail
{

	public class MenuListData : List<MenuItem>
	{
		public MenuListData ()
		{
			this.Add (new MenuItem () {
				Title = "Home",
				//IconSource = "contracts.png",
				TargetType = typeof(HomePage)
			});

			this.Add (new MenuItem () {
				Title = "Planned Calls",
				IconSource = "contracts.png",
				TargetType = typeof(PlannedPage)
			});

			this.Add (new MenuItem () {
				Title = "Sync",
				IconSource = "contracts.png",
				TargetType = typeof(SyncPage)
			});

			this.Add (new MenuItem () {
				Title = "Settings",
				IconSource = "contracts.png",
				TargetType = typeof(SettingsPage)
			});

			this.Add (new MenuItem () {
				Title = "Messages",
				//IconSource = "contracts.png",
				TargetType = typeof(MessagesPage)
			});

			this.Add (new MenuItem () {
				Title = "Help",
				//IconSource = "contracts.png",
				TargetType = typeof(HelpPage)
			});
		}
	}
}