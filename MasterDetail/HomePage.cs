using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace MasterDetail
{
	public class HomePage : ContentPage
	{
		public HomePage ()
		{
			Title = "OPUS";
			//Icon = "Contracts.png";

			Content = new Label {
				Text = "Welcome to OPUS Version 3",
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
			};
		}
	}
	
}