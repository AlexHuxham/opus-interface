using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace MasterDetail
{

	public class MessagesPage : ContentPage
	{
		public MessagesPage ()
		{
			Title = "Messages";

			var browser = new WebView ();

			browser.Source = "http://www.gekko-uk.com/OpusMessages/index.html";

			Content = browser;


		}
	}
	
}