using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace MasterDetail
{

	public class PlannedPage : ContentPage
	{
		public PlannedPage ()
		{
			Title = "PLANNED";
		

			ToolbarItems.Add(new ToolbarItem {
				Text = "+",
				Order = ToolbarItemOrder.Primary,
				Command = new Command(() => Navigation.PushAsync(new CreateCallPage()))
			});

		ToolbarItems.Add(new ToolbarItem {
				Text = "=",
				Order = ToolbarItemOrder.Primary,
				Command = new Command(() => Navigation.PushAsync(new QuestionnaireBasePage()))
			}); 
		}
	}
	
}