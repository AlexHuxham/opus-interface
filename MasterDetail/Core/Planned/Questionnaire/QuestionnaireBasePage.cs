﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace MasterDetail
{
	public class QuestionnaireBasePage : ContentPage
	{
		public QuestionnaireBasePage ()
		{
			Title = "QUESTIONNAIRE";

			ScrollView mainView = new ScrollView {
				VerticalOptions = LayoutOptions.FillAndExpand,

				Content = new Label {

					Text = "Today is Tuesday the 6th of January" +
					"Today is Tuesday the 6th of January" +
					"Today is Tuesday the 6th of January" +
					"Today is Tuesday the 6th of January" +
					"Today is Tuesday the 6th of January" +
					"Today is Tuesday the 6th of January" +
					"Today is Tuesday the 6th of January" +
					"Today is Tuesday the 6th of January" +
					"Today is Tuesday the 6th of January" +
					"Today is Tuesday the 6th of January" +
					"\n\n" +
					"Today is Tuesday the 6th of January" +
					"Today is Tuesday the 6th of January" +
					"Today is Tuesday the 6th of January" +
					"Today is Tuesday the 6th of January" +
					"Today is Tuesday the 6th of January" +
					"Today is Tuesday the 6th of January" +
					"\n\n" +
					"Today is Tuesday the 6th of January" +
					"Today is Tuesday the 6th of January" +
					"Today is Tuesday the 6th of January" +
					"Today is Tuesday the 6th of January",
					FontSize = (12),
					FontFamily = "Avenir"
				}

			};

			this.Content = new StackLayout {
				Padding = new Thickness (15, 15, 15, 0),
				Children = {
					mainView
				}
			};
		}
	}
}