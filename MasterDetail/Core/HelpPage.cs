using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace MasterDetail
{

	public class HelpPage : ContentPage
	{
		public HelpPage ()
		{
			Title = "HELP";

			var browser = new WebView ();

			browser.Source = "http://www.gekko-uk.com/field_marketing_agency/Press/Press-MarketingWeek-ConnectedHomeResearch.pdf";

			Content = browser;
		}
	}
	
}