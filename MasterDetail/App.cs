﻿using System;
using Xamarin.Forms;

namespace MasterDetail
{
	public class App : Application
	{
		public static Page GetMainPage ()
		{	
			return new RootPage ();

		}
	}
}